#2018-07-11
alter table arena add ts_nbr varchar(10) DEFAULT NULL comment 'diarienummer hos transportstyrelsen för registrering av modellflygfält' after smff_nbr;

# 2018-05-18
alter table attachment add attribute_id int unsigned after arena_id;
