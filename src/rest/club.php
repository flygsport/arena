<?php
require_once ($_SERVER ['DOCUMENT_ROOT'] . '/arena/rest/include/DBOperation.php');

class Club extends RESTfulOperation{
	private $key = 'id';
	private $columns = Array('sf', 'df', 'name', 'name2', 'org_nr', 'rf_nr', 
	                 'idrott', 'distrikt', 'gren', 'kommun', 'epost', 'hemsida', 'active');

	public function read() {
    $key = $this->getParameterOrNull ( $this->key );
		$stmt = null;
		if ($key != null) {
			$query = 'select '. $this->key. ', '. implode($this->columns, ', ') . ' from club where ' . $this->key . '=?';
			$stmt = $this->mysqli->prepare ( $query );
			$stmt->bind_param ( 's', $key );
		} else {
		  $query = 'select '. $this->key. ', '. implode($this->columns, ', ') . ' from club order by name';
		  $stmt = $this->mysqli->prepare ( $query );
		}
		$this->executeAndCloseQuery ( $stmt );
  }

	//----- create() --------------------------------------------------------------
	public function create(){
		$query = 'INSERT INTO club SET ';
		$values = Array();
		foreach($this->columns as $column){
			$values[$column] =& $this->parameters [$column];
			$query .= $column . '=?,';
		}
		$query = substr($query, 0, -1);
		$stmt = $this->mysqli->prepare ($query);
		$params = array_merge(array(str_repeat('s', count($values))), array_values($values));
		call_user_func_array(array(&$stmt, 'bind_param'), $params);
		$stmt->execute ();
		$clubId = $this->mysqli->insert_id;
		$this->parameters [$this->key] = $clubId;
		$this->read();
	}

	//----- replace() --------------------------------------------------------------
	public function replace(){
		$query = 'UPDATE club SET ';
		$values = Array();
		foreach($this->columns as $column){
			$values[$column] =& $this->parameters [$column];
			$query .= $column . '=?,';
		}
		$values[$this->key] =& $this->parameters [$this->key];
		$query = substr($query, 0, -1);
		$query .= ' WHERE ' . $this->key . '=?';
		$stmt = $this->mysqli->prepare ($query);
		$params = array_merge(array(str_repeat('s', count($values))), array_values($values));
		call_user_func_array(array(&$stmt, 'bind_param'), $params);
		$stmt->execute ();
		$this->read();
	}

}

(new Club())->generateResponse();

?>
