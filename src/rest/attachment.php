<?php
require_once ($_SERVER ['DOCUMENT_ROOT'] . '/arena/rest/include/DBOperation.php');
/**
 *    Thrown when a file upload error occurs.
 *  	php error code, see http://php.net/manual/en/features.file-upload.errors.php
 **/

class Upload extends RESTfulOperation{
	public function create(){
		if (isset($_FILES['file']) && ($_FILES["file"]["error"] == UPLOAD_ERR_OK)
		    && (is_uploaded_file($_FILES['file']['tmp_name']))) {
			$arena = $_POST['arena'];
			$attribute = $_POST['attribute'];
			$mime = $_FILES['file']['type'];
			$originalName = $_FILES['file']['name'];

			$sql = "INSERT attachment SET arena_id=?, attribute_id=?, mime_type=?, file_name=?, file=?";
			$stmt = $this->mysqli->prepare($sql);
			$null = NULL;
			$stmt->bind_param("ssssb", $arena, $attribute, $mime, $originalName, $null);
			$fp = fopen($_FILES['file']['tmp_name'], "r");
			while (!feof($fp)) {
				$stmt->send_long_data(4, fread($fp, 8192));
			}
			fclose($fp);
			$stmt->execute();
			$this->result = array(
				'status'        => true,
				'originalName'  => $originalName,
				'mime' => $mime,
				'arena' => $arena
			);
		} else {
			throw new UploadException($_FILES["file"]["error"]);
			exit;
		}
	}
		
		// ----- read() --------------------------------------------------------------
	public function read() {
		$arena = $this->getParameterOrNull ( 'arena_id' );
		if ($arena != null) {
			// list all files for the arena
			$query = ' select id, arena_id, attribute_id, mime_type, file_name, created from attachment where arena_id=?';
			$stmt = $this->mysqli->prepare ( $query );
			$stmt->bind_param ( 's', $arena );
			$this->executeAndCloseQuery ( $stmt );
		} else {
			$id = $this->getParameterOrNull ( 'id' );
			if ($id != null) {
 			  $query = "SELECT mime_type, file FROM attachment WHERE id=?";
 			  $stmt = $this->mysqli->prepare ( $query );
 			  $stmt->bind_param ( 's', $id );
 			  $stmt->execute ();
 			  $res = $stmt->get_result ();
 			  $i = 0;
 			  if ( $row = $res->fetch_assoc () ) {
 			  	header("Content-type: " . $row['mime_type']);
			    echo $row['file'];
			    exit();
 			  }
 			  throw new RESTfulException("file not found", "file not found");
			}
		}
	}
}

class UploadException extends RESTfulException {
	public function __construct($code){
		if($code == UPLOAD_ERR_INI_SIZE){
			// NOTE, this do not occure since size is checked in DBOperation.generateResponse() during json decode, before create() is called
			parent::__construct('The upload failed. The file is to large. Max size is ' . RESTfulOperation::humanFilesize ( RESTfulOperation::fileUploadMaxSize () ) . '.', // $userErrorMessage,
					'The upload failed. The file is to large. Max size is ' . RESTfulOperation::humanFilesize ( RESTfulOperation::fileUploadMaxSize () ) . '.', // $developerErrorMessage,
					RESTfulException::HTTP_STATUS_REQUEST_ENTITY_TO_LARGE, // $httpStatusCode,
					'file to large]', // $httpStatusText,
					null // $previous)
					);
		} else {
			$message = $this->codeToMessage($code);
			parent::__construct('Server side error while uploading file. ', // $userErrorMessage,
					'PHP indcated an error while uploading file. ' + $message, // $developerErrorMessage,
					RESTfulException::HTTP_STATUS_INTERNAL_SERVER_ERROR, // $httpStatusCode,
					'upload error]', // $httpStatusText,
					null // $previous)
					);
		}
	}
	
	private function codeToMessage($code) {
		switch ($code) {
			case UPLOAD_ERR_INI_SIZE:
				$message = "The uploaded file exceeds the upload_max_filesize directive in php.ini";
				break;
			case UPLOAD_ERR_FORM_SIZE:
				$message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
				break;
			case UPLOAD_ERR_PARTIAL:
				$message = "The uploaded file was only partially uploaded";
				break;
			case UPLOAD_ERR_NO_FILE:
				$message = "No file was uploaded";
				break;
			case UPLOAD_ERR_NO_TMP_DIR:
				$message = "Missing a temporary folder";
				break;
			case UPLOAD_ERR_CANT_WRITE:
				$message = "Failed to write file to disk";
				break;
			case UPLOAD_ERR_EXTENSION:
				$message = "File upload stopped by extension";
				break;
				
			default:
				$message = "Unknown upload error";
				break;
		}
		return $message;
	}
}


(new Upload())->generateResponse();

?>
