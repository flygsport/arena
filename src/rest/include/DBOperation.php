<?php
/********************************************************************************
 *
 *  Base class for a RESTful operation and database access.
 *  Store your result in the associative array $this->result.
 *  The array will be JSON encoded and returned to the caller.
 *
 *  To return an error message, throw an RESTfulException, define at the bottom of this file.
 *
 *  The response body always contain a JSON object
 *  (except on fatal and other non catchable errors, such as php parse error)
 *  The response object contains one object with a field named success.
 *  When success === true, the reply object type is:
 *    {
 *      success:boolean,
 *      data:any
 *     }
 *  When success === false, the reply object type is:
 *    {
 *      success:boolean,
 *      userErrorMessageL string,
 *      developerErrorMessage: string,
 *      stackTrace: string
 *      url: string
 *    }
 *
 *  USAGE:
 *  Override the opertions you support in a subclass and call
 *  RESTfulOperation::generateResponse() on an instance of your subclass.
 *  example: (new Course())->generateResponse();
 *
 *  Methods to override:
 *
 *    public function replace()  // PUT, must be idempotent
 *	  public function create()   // POST
 *	  public function read()     // GET, safe method (has no side effects)
 *	  public function delete()   //DELETE, must be idempotent
 *    public function update()   //PATCH
 *
 *    Default behavior for the methods above is to return 501 - not implemented.
 *
 *  Other helpful methods:
 *
 *    protected function executeSelect($query)
 *      Execute a sql statement and add the result to $this->result.
 *      $query - a string containing sql query
 *
 * 	  protected function executeAndCloseQuery($stmt)
 *      Execute a sql statement and add the result to $this->result.
 *      $stmt - a mysqli_stmt created by mysqli::prepare ( string $query ) ready to execute.
 *
 *
 *******************************************************************************/

/**
 * ******************************************************************************
 *
 * RESTful parameter formating style guilde
 * see, for example http://www.vinaysahni.com/best-practices-for-a-pragmatic-restful-api
 *
 * - keep it simple
 * - parameters are by default URL encoded
 * - use a plural
 * - use hypen-separation in parameter names, i.e. student-id
 * *****************************************************************************
 */

/**
 * ******************************************************************************
 *
 * JSON formating style guilde
 * from https://google.github.io/styleguide/jsoncstyleguide.xml#Property_Name_Format
 *
 * Property names must conform to the following guidelines:
 * - Property names should be meaningful names with defined semantics.
 * - Property names must be camel-cased, ascii strings.
 * - The first character must be a letter, an underscore (_) or a dollar sign ($).
 * - Subsequent characters can be a letter, a digit, an underscore, or a dollar sign.
 * - Reserved JavaScript keywords should be avoided.
 * *****************************************************************************
 */

/**
 * ******************************************************************************
 * Import configuration
 */
require_once ($_SERVER ['DOCUMENT_ROOT'] . '/arena/rest/include/config.php');
class RESTfulOperation {
	protected $productionServer = true;

    // request
	protected $parameters; // values sent as part of the request, associative array, i.e. value=$parameters[key]
	                       
	// internal state
	protected $mysqli = NULL;
	
	// respons - head
	// default "HTTP/1.1 200 OK"
	private $httpStatus = RESTfulException::HTTP_STATUS_OK; // se comment at bottom of file for a list of values
	private $httpStatusText = 'OK'; // sent as part of the response head first line
	// response - body
	protected $result = array (); // during normal operation, this array is JSON encoded and returned in the body of the response
	private $userErrorMessage = "Unknown Error";
	private $developerErrorMessage = "Unknown Error";
	private $stackTrace = "";
	
	/**
	 * RESTful API, override in subclasses
	 */
	public function replace() { // PUT, must be idempotent
		throw new RESTfulException ( 'PUT is not implemented', 'PUT is not implemented', RESTfulException::HTTP_STATUS_NOT_IMPLEMENTED, false );
	}
	public function create() { // POST
		throw new RESTfulException ( 'POST is not implemented', 'POST is not implemented', RESTfulException::HTTP_STATUS_NOT_IMPLEMENTED, false );
	}
	public function read() { // GET, safe method (has no side effects)
		throw new RESTfulException ( 'GET is not implemented', 'GET is not implemented', RESTfulException::HTTP_STATUS_NOT_IMPLEMENTED, false );
	}
	public function delete() { // DELETE, must be idempotent
		throw new RESTfulException ( 'DELETE is not implemented', 'DELETE is not implemented', RESTfulException::HTTP_STATUS_NOT_IMPLEMENTED, false );
	}
	public function update() { // PATCH
		throw new RESTfulException ( 'PATCH is not implemented', 'PATCH is not implemented', RESTfulException::HTTP_STATUS_NOT_IMPLEMENTED, false );
	}
	
	/**
	 * Call generateResponse() to generate the response and send it to the client
	 * - connect to the database
	 * - call the proper server function, i.e.
	 * replace(), create(), read(), delete(), or update()
	 * - clode the database
	 * - sent the response to the client
	 */
	public function generateResponse() {
		try {
			$this->productionServer = array_key_exists('SAM_PRODUCTION_SERVER', $_SERVER);
			$this->start_http_session ();
			$this->connectToDB ();
			
			switch ($_SERVER ['REQUEST_METHOD']) {
				case 'PUT' : // Update/Replace
					$this->prepareJsonParameters();
					$this->replace ();
					break;
				case 'POST' : // create
					$this->prepareJsonParameters();
					$this->create ();
					break;
				case 'GET' : // Read
					$this->parameters = $_GET;
					$this->read ();
					break;
				case 'DELETE' : // Delete
					$this->parameters = $_GET;
					$this->delete ();
					break;
				case 'PATCH' : // Update (partial object)
					$this->prepareJsonParameters();
					$this->update ();
					break;
			}
		} catch ( RESTfulException $e ) {
			$this->httpStatus = $e->getHttpStatusCode ();
			$this->httpStatusText = $e->getHttpStatusText ();
			$this->userErrorMessage = $e->getUserErrorMessage ();
			if (!$this->productionServer) { // this is only usefull fo debugging, do not leak inernal structure to the public
				$this->developerErrorMessage = $e->getDeveloperErrorMessage ();
				$this->stackTrace = $e->getTraceAsString ();
			}
		} catch ( Exception $e ) {
			$this->httpStatus = RESTfulException::HTTP_STATUS_INTERNAL_SERVER_ERROR;
			$this->httpStatusText = "Internal server error";
			$this->userErrorMessage = "Internal server error, " . $e->getMessage ();
			if (!$this->productionServer) { // this is only usefull fo debugging, do not leak inernal structure to the public
				$this->developerErrorMessage = "Internal server error, uncaught exception: " . $e->getMessage ();
				$this->stackTrace = $e->getTraceAsString ();
			}
		}
		// finally is not supported in php 5.4 finally{
		$this->disconnectFromDB ();
		// }
		
		// send http header
		header ( $_SERVER ["SERVER_PROTOCOL"] . ' ' . $this->httpStatus . ' ' . $this->httpStatusText );
		header ( 'Content-Type: application/json' );
		
		// send http body
		if ($this->httpStatus >= 200 && $this->httpStatus <= 299) {
			// success, JSON encode result
			$data = array ();
			$data ['success'] = true;
			$data ['data'] = $this->result;
			echo json_encode ( $data );
		} else {
			// failure, JSON encode error message
			// Get HTTP/HTTPS (the possible values for this vary from server to server)
			$myUrl = (isset ( $_SERVER ['HTTPS'] ) && $_SERVER ['HTTPS'] && ! in_array ( strtolower ( $_SERVER ['HTTPS'] ), array (
					'off',
					'no' 
			) )) ? 'https' : 'http';
			// Get domain portion
			$myUrl .= '://' . $_SERVER ['HTTP_HOST'];
			// Get path to script
			$myUrl .= $_SERVER ['REQUEST_URI'];
			// Add path info, if any
			if (! empty ( $_SERVER ['PATH_INFO'] )) {
				$myUrl .= $_SERVER ['PATH_INFO'];
			}
			// Add query string, if any (some servers include a ?, some don't)
			if (! empty ( $_SERVER ['QUERY_STRING'] )) {
				$myUrl .= '?' . ltrim ( $_SERVER ['REQUEST_URI'], '?' );
			}
			;
			
			$data = array ();
			$data ['url'] = $myUrl;
			$data ['success'] = false;
			$data ['userErrorMessage'] = $this->userErrorMessage;
			$data ['developerErrorMessage'] = $this->developerErrorMessage;
			$data ['stackTrace'] = $this->stackTrace;
			echo json_encode ( $data );
		}
		echo "\n"; // not needed, but nice when run from terminal , i.e. curl -D - --data "param1=value1&param2=value2" http://localhost/portal/php/course.php
	}
	
	protected function prepareJsonParameters(){
		// Normally the POST/PUT body contains all parameters in JSON
		// When doing a file upload through POST, multipart/form-data, the body contains the file, do not json-decode it.
		if (! isset ( $_FILES ['file'] )) {
			// $_FILES is not set if the file is to large, check the content length
			if (! isset ( $_SERVER ['CONTENT_LENGTH'] ) || (( int ) $_SERVER ['CONTENT_LENGTH']) < $this->fileUploadMaxSize ()) {
				$this->parameters = ( array ) json_decode ( file_get_contents ( "php://input" ), true );
			} else {
				throw new RESTfulException ( 'The upload failed. The file is to large. Max size is ' . RESTfulOperation::humanFilesize ( RESTfulOperation::fileUploadMaxSize () ) . '.', 'The upload failed. The file is to large. Max size is ' . RESTfulOperation::humanFilesize ( RESTfulOperation::fileUploadMaxSize () ) . '.', RESTfulException::HTTP_STATUS_REQUEST_ENTITY_TO_LARGE, false );
			}
		}
	}
	/**
	 * *---------------- helper database functions, to make life easy -------------------**
	 */
	
	/**
	 * Execute a sql query and put the result in $this->result.
	 * Use executeAndCloseQuery($stmt) if you need to use prepared statements.
	 * $query - a string containg the sql code to execute.
	 */
	protected function executeSelect($query) {
		$stmt = $this->mysqli->prepare ( $query );
		$this->executeAndCloseQuery ( $stmt );
	}
	
	/**
	 * Given a mysqli statement, execute it, put the result in $this->result and close the mysqli statement.
	 * $stmt - a myqli prepared statement ready to execute.
	 */
	protected function executeAndCloseQuery($stmt) {
		$stmt->execute ();
		$res = $stmt->get_result ();
		$i = 0;
		while ( $row = $res->fetch_assoc () ) {
			$res_row = array ();
			foreach ( $row as $key => $value ) {
				$res_row [$key] = $value;
			}
			$this->result [] = $res_row;
		}
		$stmt->close ();
	}
	
	/**
	 * Start a http session if run within a web server.
	 * This must be done before anny data is sent to the client.
	 */
	function start_http_session() {
		if (php_sapi_name () !== 'cli') {
			if (version_compare ( phpversion (), '5.4.0', '>=' )) {
				if (session_status () !== PHP_SESSION_ACTIVE) {
					session_start ();
				}
			} else {
				if (session_id () === '') {
					session_start ();
				}
			}
		} else {
			// things to do when run from cli, i.e. create state for unit test
			$_SESSION = array ();
		}
	}
	
	/**
	 * Connect to the database.
	 * Make mysqli throw exeptions on errors.
	 * from php 5.3.4 this setting is per request (earlier it was per thread so it could contaminate other http requests)
	 */
	function connectToDB() {
		mysqli_report ( MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT );
		global $MYSQL_HOST;
		global $MYSQL_USER;
		global $MYSQL_PWD;
		global $MYSQL_DATABASE;
		// all mysqli calls throws exceptions on errors
		$this->mysqli = new mysqli ( $MYSQL_HOST, $MYSQL_USER, $MYSQL_PWD, $MYSQL_DATABASE );
		$this->mysqli->set_charset ( 'utf8' ); // IMPORTANT, JSON response must be utf8 encoded.
	}
	function disconnectFromDB() {
		if ($this->mysqli != NULL) {
			$this->mysqli->close ();
			$this->mysqli = NULL;
		}
	}
	public function boolToInt($val){
		if(is_null($val)){
				return NULL;
		} else if($val){
				return 1;
		}
		return 0;
  }
	public function getParameter($key) {
		if (isset ( $this->parameters [$key] )) {
			return $this->parameters [$key];
		} else {
			throw new RESTfulException ( "missing parameter '$key'", "missing parameter $key", RESTfulException::HTTP_STATUS_PRECONDITION_FAILED, false );
		}
	}
	public function getParameterOrNull($key) {
		if (isset ( $this->parameters [$key] )) {
			return $this->parameters [$key];
		} else {
			return null;
		}
	}
	public function hasParameter($key) {
		return isset ( $this->parameters [$key] );
	}
	
	// Returns a file size limit in bytes based on the PHP upload_max_filesize
	// and post_max_size
	public static function fileUploadMaxSize() {
		static $max_size = - 1;
		
		if ($max_size < 0) {
			// Start with post_max_size.
			$max_size = RESTfulOperation::parseSize ( ini_get ( 'post_max_size' ) );
			
			// If upload_max_size is less, then reduce. Except if upload_max_size is
			// zero, which indicates no limit.
			$upload_max = RESTfulOperation::parseSize ( ini_get ( 'upload_max_filesize' ) );
			if ($upload_max > 0 && $upload_max < $max_size) {
				$max_size = $upload_max;
			}
		}
		return $max_size;
	}
	
	// convert a php.ini size string to integer, i.e. 1m => 1024
	public static function parseSize($size) {
		$unit = preg_replace ( '/[^bkmgtpezy]/i', '', $size ); // Remove the non-unit characters from the size.
		$size = preg_replace ( '/[^0-9\.]/', '', $size ); // Remove the non-numeric characters from the size.
		if ($unit) {
			// Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
			return round ( $size * pow ( 1024, stripos ( 'bkmgtpezy', $unit [0] ) ) );
		} else {
			return round ( $size );
		}
	}
	
	// convert an integer size to human readable format, i.e. 1024 => 1MB
	public static function humanFilesize($size, $decimals = 2) {
		$units = array (
				'B',
				'kB',
				'MB',
				'GB',
				'TB',
				'PB',
				'EB',
				'ZB',
				'YB' 
		);
		$step = 1024;
		$i = 0;
		while ( ($size / $step) > 0.9 ) {
			$size = $size / $step;
			$i ++;
		}
		return round ( $size, $decimals ) . $units [$i];
	}
}

/**
 * ****************************************************
 * Throw a RESTfulException to make RESTfulOperation generate a http error response.
 * ****************************************************
 */
class RESTfulException extends Exception {
	private $httpStatusCode;
	private $httpStatusText;
	private $userErrorMessage;
	private $developerErrorMessage;
	public function __construct($userErrorMessage, $developerErrorMessage, $httpStatusCode = self::HTTP_STATUS_INTERNAL_SERVER_ERROR, $httpStatusText = "internal server error", $previous = null) {
		$this->userErrorMessage = $userErrorMessage;
		$this->developerErrorMessage = $developerErrorMessage;
		$this->httpStatusCode = $httpStatusCode;
		$this->httpStatusText = $httpStatusText;
		parent::__construct ( $developerErrorMessage, $httpStatusCode, $previous );
	}
	public function getUserErrorMessage() {
		return $this->userErrorMessage;
	}
	public function getDeveloperErrorMessage() {
		return $this->developerErrorMessage;
	}
	public function getHttpStatusCode() {
		return $this->httpStatusCode;
	}
	public function getHttpStatusText() {
		return $this->httpStatusText;
	}
	
	/**
	 * ************************************************
	 * http Status-Codes
	 * https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
	 * ************************************************
	 */
	const HTTP_STATUS_CONTINUE = 100; // Section 10.1.1: Continue
	const HTTP_STATUS_SWITCH_PROTOCOL = 101; // Section 10.1.2: Switching Protocols
	const HTTP_STATUS_OK = 200; // Section 10.2.1: OK
	const HTTP_STATUS_CREATED = 201; // Section 10.2.2: Created
	const HTTP_STATUS_ACCEPTED = 202; // Section 10.2.3: Accepted
	const HTTP_STATUS_NON_AUTHORITATIVE_INFO = 203; // Section 10.2.4: Non-Authoritative Information
	const HTTP_STATUS_NO_CONTENT = 204; // Section 10.2.5: No Content
	const HTTP_STATUS_RESET_CONTENT = 205; // Section 10.2.6: Reset Content
	const HTTP_STATUS_PARTIAL_CONTENT = 206; // Section 10.2.7: Partial Content
	const HTTP_STATUS_MULTIPLE_CHOISE = 300; // Section 10.3.1: Multiple Choices
	const HTTP_STATUS_MOVED_PERMANENTLY = 301; // Section 10.3.2: Moved Permanently
	const HTTP_STATUS_FOUND = 302; // Section 10.3.3: Found
	const HTTP_STATUS_SEE_OTHER = 303; // Section 10.3.4: See Other
	const HTTP_STATUS_NOT_MODIFIED = 304; // Section 10.3.5: Not Modified
	const HTTP_STATUS_USE_PROXY = 305; // Section 10.3.6: Use Proxy
	const HTTP_STATUS_TEMPORARY_REDIRECT = 307; // Section 10.3.8: Temporary Redirect
	const HTTP_STATUS_BAD_REQUEST = 400; // Section 10.4.1: Bad Request
	const HTTP_STATUS_UNAUTHORIZED = 401; // Section 10.4.2: Unauthorized
	const HTTP_STATUS_PAYMENT_REQUIRED = 402; // Section 10.4.3: Payment Required
	const HTTP_STATUS_FORBIDDEN = 403; // Section 10.4.4: Forbidden
	const HTTP_STATUS_NOT_FOUND = 404; // Section 10.4.5: Not Found
	const HTTP_STATUS_METHOD_NOT_ALLOWED = 405; // Section 10.4.6: Method Not Allowed
	const HTTP_STATUS_NOT_ACCEPTABLE = 406; // Section 10.4.7: Not Acceptable
	const HTTP_STATUS_PROXY_AUTHENTICATION_REQUIRED = 407; // Section 10.4.8: Proxy Authentication Required
	const HTTP_STATUS_REQUEST_TIMEOUT = 408; // Section 10.4.9: Request Time-out
	const HTTP_STATUS_CONFLICT = 409; // Section 10.4.10: Conflict
	const HTTP_STATUS_GONE = 410; // Section 10.4.11: Gone
	const HTTP_STATUS_LENGTH_REQUIRED = 411; // Section 10.4.12: Length Required
	const HTTP_STATUS_PRECONDITION_FAILED = 412; // Section 10.4.13: Precondition Failed
	const HTTP_STATUS_REQUEST_ENTITY_TO_LARGE = 413; // Section 10.4.14: Request Entity Too Large
	const HTTP_STATUS_REQUEST_URI_TO_LARGE = 414; // Section 10.4.15: Request-URI Too Large
	const HTTP_STATUS_UNSUPPORTED_MEDIA_TYPE = 415; // Section 10.4.16: Unsupported Media Type
	const HTTP_STATUS_REQUEST_RANGE_NOT_SATISFIABLE = 416; // Section 10.4.17: Requested range not satisfiable
	const HTTP_STATUS_EXCEPTION_FAILED = 417; // Section 10.4.18: Expectation Failed
	const HTTP_STATUS_INTERNAL_SERVER_ERROR = 500; // Section 10.5.1: Internal Server Error
	const HTTP_STATUS_NOT_IMPLEMENTED = 501; // Section 10.5.2: Not Implemented
	const HTTP_STATUS_BAD_GATEWAY = 502; // Section 10.5.3: Bad Gateway
	const HTTP_STATUS_SERVICE_UNAVAILABLE = 503; // Section 10.5.4: Service Unavailable
	const HTTP_STATUS_GATEWAY_TIMEOUT = 504; // Section 10.5.5: Gateway Time-out
	const HTTP_STATUS_HTTP_VERSION_NOT_SUPPORTED = 505; // Section 10.5.6: HTTP Version not supported
}
?>
