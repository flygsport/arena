<?php
require_once ($_SERVER ['DOCUMENT_ROOT'] . '/arena/rest/include/DBOperation.php');

class Arena extends RESTfulOperation{
	//----- create() --------------------------------------------------------------
	public function create(){
		$name = $this->getParameterOrNull('name');
		$name2 = $this->getParameterOrNull('name2');
		$kind = $this->getParameterOrNull('kind');
		
		$longitude = $this->getParameter('longitude');
		$latitude = $this->getParameter('latitude');
		$altitude_ground = $this->getParameterOrNull('altitude_ground');
		$radius = $this->getParameterOrNull('radius');
		$height = $this->getParameterOrNull('height');
		$official_map_arena = $this->boolToInt($this->getParameterOrNull('official_map_arena'));

		$aerobatic_box_bottom = $this->getParameterOrNull('aerobaticBoxBottom');
		$aerobatic_box_top = $this->getParameterOrNull('aerobaticBoxTop');

		$radio = $this->getParameterOrNull('radio');
		$phone = $this->getParameterOrNull('phone');
		$url = $this->getParameterOrNull('url');
		$email = $this->getParameterOrNull('email');
		
		$icao = $this->getParameterOrNull('icao');
		$icao_deprecated = $this->boolToInt($this->getParameterOrNull('icao_deprecated'));
		$smff_nbr = $this->getParameterOrNull('smff_nbr');
		$ts_nbr = $this->getParameterOrNull('ts_nbr');
		
		$landing = $this->boolToInt($this->getParameterOrNull('landing'));
		$take_off = $this->boolToInt($this->getParameterOrNull('take_off'));
		$in_air = $this->boolToInt($this->getParameterOrNull('in_air'));
		
		$aerobatic = $this->boolToInt($this->getParameterOrNull('aerobatic'));
		$aeromodeling = $this->boolToInt($this->getParameterOrNull('aeromodeling'));
		$aviation = $this->boolToInt($this->getParameterOrNull('aviation'));
		$balooning = $this->boolToInt($this->getParameterOrNull('balooning'));
		$gliding = $this->boolToInt($this->getParameterOrNull('gliding'));
		$hanggliding = $this->boolToInt($this->getParameterOrNull('hanggliding'));
		$parachuting = $this->boolToInt($this->getParameterOrNull('parachuting'));
		$paragliding = $this->boolToInt($this->getParameterOrNull('paragliding'));
		
		$comment = $this->getParameterOrNull('comment');
		
		$regulations = $this->getParameterOrNull('local_regulations');
		
		$query = 'insert into arena set name=?, name2=?, kind=?, icao=?, icao_deprecated=?, smff_nbr=?, ts_nbr=? '
					 . ', longitude=?, latitude=?, altitude_ground=?, radius=?, height=?, official_map_arena=? '
					 . ', aerobatic_box_bottom=?, aerobatic_box_top=?'
           . ', url=?, email=?, radio=?, phone=? '
           . ', landing=?, take_off=?, in_air=? '
           . ', aerobatic=?, aeromodeling=?, aviation=?, balooning=? '
           . ', gliding=?, hanggliding=?, parachuting=?, paragliding=? '
           . ', comment=?, local_regulations=?, created=now()';

		$stmt = $this->mysqli->prepare ( $query );
		$stmt->bind_param ( 'ssssssssssssssssssssssssssssssss',
                    $name, $name2, $kind, $icao, $icao_deprecated, $smff_nbr, $ts_nbr,
										$longitude, $latitude, $altitude_ground, $radius, $height, $official_map_arena,
										$aerobatic_box_bottom, $aerobatic_box_top,
				            $url, $email, $radio, $phone,
				            $landing, $take_off, $in_air,
				            $aerobatic, $aeromodeling, $aviation, $balooning,
				            $gliding, $hanggliding, $parachuting, $paragliding,
		                $comment, $regulations);
		$stmt->execute ();
		$arenaId = $this->mysqli->insert_id;
		$this->parameters ['id'] = $arenaId;
		$clubList = $this->getParameterOrNull('clubList');
		$this->updateClubList($arenaId, $clubList);
		$this->read();
	}
	
	//----- replace() --------------------------------------------------------------
	public function replace(){
		$id = $this->getParameterOrNull('id');
		$name = $this->getParameterOrNull('name');
		$name2 = $this->getParameterOrNull('name2');
		$kind = $this->getParameterOrNull('kind');
		
		$longitude = $this->getParameter('longitude');
		$latitude = $this->getParameter('latitude');
		$altitude_ground = $this->getParameterOrNull('altitude_ground');
		$radius = $this->getParameterOrNull('radius');
		$height = $this->getParameterOrNull('height');
		$official_map_arena = $this->boolToInt($this->getParameterOrNull('official_map_arena'));

		$aerobatic_box_bottom = $this->getParameterOrNull('aerobaticBoxBottom');
		$aerobatic_box_top = $this->getParameterOrNull('aerobaticBoxTop');

		$radio = $this->getParameterOrNull('radio');
		$phone = $this->getParameterOrNull('phone');
		$url = $this->getParameterOrNull('url');
		$email = $this->getParameterOrNull('email');
		
		$icao = $this->getParameterOrNull('icao');
		$icao_deprecated = $this->boolToInt($this->getParameterOrNull('icao_deprecated'));
		$smff_nbr = $this->getParameterOrNull('smff_nbr');
		$ts_nbr = $this->getParameterOrNull('ts_nbr');

		$landing = $this->boolToInt($this->getParameterOrNull('landing'));
		$take_off = $this->boolToInt($this->getParameterOrNull('take_off'));
		$in_air = $this->boolToInt($this->getParameterOrNull('in_air'));
		
		$aerobatic = $this->boolToInt($this->getParameterOrNull('aerobatic'));
		$aeromodeling = $this->boolToInt($this->getParameterOrNull('aeromodeling'));
		$aviation = $this->boolToInt($this->getParameterOrNull('aviation'));
		$balooning = $this->boolToInt($this->getParameterOrNull('balooning'));
		$gliding = $this->boolToInt($this->getParameterOrNull('gliding'));
		$hanggliding = $this->boolToInt($this->getParameterOrNull('hanggliding'));
		$parachuting = $this->boolToInt($this->getParameterOrNull('parachuting'));
		$paragliding = $this->boolToInt($this->getParameterOrNull('paragliding'));
		
		$comment = $this->getParameterOrNull('comment');
		$regulations = $this->getParameterOrNull('local_regulations');
		
		$query = 'update arena set name=?, name2=?, kind=?, icao=?, icao_deprecated=?, smff_nbr=?, ts_nbr=? '
			   . ', longitude=?, latitude=?, altitude_ground=?, radius=?, height=?, official_map_arena=? '
				 . ', aerobatic_box_bottom=?, aerobatic_box_top=?'
         . ', url=?, email=?, radio=?, phone=? '
			   . ', landing=?, take_off=?, in_air=? '
			   . ', aerobatic=?, aeromodeling=?, aviation=?, balooning=? '
			   . ', gliding=?, hanggliding=?, parachuting=?, paragliding=? '
			   . ', comment=?, local_regulations=?, last_edit=now() where id=?';
										
		$stmt = $this->mysqli->prepare ( $query );
		$stmt->bind_param ( 'sssssssssssssssssssssssssssssssss',
            $name, $name2, $kind, $icao, $icao_deprecated, $smff_nbr, $ts_nbr,
            $longitude, $latitude, $altitude_ground, $radius, $height, $official_map_arena,
						$aerobatic_box_bottom, $aerobatic_box_top,
            $url, $email, $radio, $phone,
            $landing, $take_off, $in_air,
            $aerobatic, $aeromodeling, $aviation, $balooning,
            $gliding, $hanggliding, $parachuting, $paragliding,
            $comment, $regulations, $id);
		$stmt->execute ();
		$clubList = $this->getParameterOrNull('clubList');
		$this->updateClubList($id, $clubList);
		$this->read();
	}
	
	//----- updateClubList() --------------------------------------------------------------
	private function updateClubList($arenaId, $clubList) {
		foreach ($clubList as $club){
			$query = 'insert into clubarena set club=?, arena=? on duplicate key update arena=arena';
			$stmt = $this->mysqli->prepare ( $query );
			$clubId = $club['id'];
			$stmt->bind_param ( 'ss', $clubId, $arenaId);
			$stmt->execute ();
		}
	}
	
	//----- read() --------------------------------------------------------------
	public function read() {
		$id = $this->getParameterOrNull ( 'id' );
		$query = 'select id, name, name2, kind, url, email, phone, radio '
		       . ', longitude, latitude, altitude_ground, radius, height, official_map_arena '
					 . ', aerobatic_box_bottom as aerobaticBoxBottom, aerobatic_box_top as aerobaticBoxTop'
		       . ', kind, comment, local_regulations, icao, icao_deprecated, smff_nbr, ts_nbr '
		       . ', landing, take_off, in_air '
		       . ', aerobatic, aeromodeling, aviation, balooning '
		       . ', gliding, hanggliding, parachuting, paragliding '
		       . ' from arena';
        if ($id != null) {
			
            $query .= ' where id=?';

            $stmt = $this->mysqli->prepare ( $query );
			$stmt->bind_param ( 's', $id );
			$this->executeSQL ( $stmt );
		} else {

		    $query .= ' order by name ';
			
			$stmt = $this->mysqli->prepare ( $query );
			$this->executeSQL ( $stmt );
			// TODO read clubList for each club
		}
	}
	
	private function executeSQL($stmt) {
      $stmt->execute ();
	  $res = $stmt->get_result ();
	  $i = 0;
	  while ( $row = $res->fetch_assoc () ) {
	    $arena_row = array ();
	    foreach ( $row as $key => $value ) {
	      $arena_row [$key] = $value;
	    }
	    $this->result [] = $arena_row;
	  }
	  
	  $nbrArenas = count($this->result);
	  for($i=0; $i < $nbrArenas; $i++){
	    // club list
	    $clubList = [];
	    $query = ' select club.id, club.name, club.sf, club.df, club.name, club.rf_nr, club.hemsida '
	        .' from club join clubarena on (club.id =clubarena.club) where arena=? '
	            . ' order by club.name ';
	    $club_stmt = $this->mysqli->prepare ( $query );
	    $arena_id = $this->result[$i]['id'];
	    $club_stmt->bind_param ( 's', $arena_id );
	    $club_stmt->execute ();
	    $res = $club_stmt->get_result ();
	    while ( $row = $res->fetch_assoc () ) {
	      $club_row = array ();
	      foreach ( $row as $key => $value ) {
	        $club_row [$key] = $value;
	      }
	      $clubList [] = $club_row;
	    }
	    $club_stmt->close ();
	    
	    $this->result[$i]['clubList'] = $clubList;
	  }
	  $stmt->close ();
	}
	    
}

(new Arena())->generateResponse();

?>
