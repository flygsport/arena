import { Directive } from '@angular/core';
import { Validator, NG_VALIDATORS, AbstractControl } from "@angular/forms";

@Directive({
  selector: '[isInteger]',
  providers: [{provide: NG_VALIDATORS, useExisting: IntegerValidatorDirective, multi: true}]

})
export class IntegerValidatorDirective implements Validator {

    validate(control: AbstractControl): {[key: string]: any} {
      const value = control.value;
      let result = null;
      if(!this.isInt(value)) {
        result = { notInteger: true };
      }
      return result;
    }

    private isInt(n){
        let value = Number(n);
        return Number.isInteger(value);
    }
}
