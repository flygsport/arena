import { Directive } from '@angular/core';
import { NG_VALIDATORS, AbstractControl } from "@angular/forms";

@Directive({
  selector: '[numeric]',
  providers: [{provide: NG_VALIDATORS, useExisting: IsNumericValidatorDirective, multi: true}]
})
export class IsNumericValidatorDirective {


    validate(control: AbstractControl): {[key: string]: any} {
      const value = control.value;
      if(!value || typeof value === 'number'){
          return null;
      }
      var re = /,/gi;
      const tmp = value.replace(re, ".");
      if(tmp !== value){
          control.setValue(tmp)
      }
      let result = null;
      if(!IsNumericValidatorDirective.isNumeric(tmp)){
        result = { notNumeric: true };
      }
      return result;
    }
    
    
    /**
     * Returns true if @param obj is a numeric value.
     * @param obj
     */
    public static isNumeric( obj: any ): boolean {
      /**
       * Code from JQuery 2016-10-03,
       * https://github.com/jquery/jquery/blob/c869a1ef8a031342e817a2c063179a787ff57239/src/core.js#L214
       * parseFloat NaNs numeric-cast false positives (null|true|false|"")
       * ...but misinterprets leading-number strings, particularly hex literals ("0x...")
       * subtraction forces infinities to NaN
       * adding 1 corrects loss of precision from parseFloat (#15100)
       */
        return !Array.isArray( obj ) && (obj - parseFloat( obj ) + 1) >= 0;
      }

}
