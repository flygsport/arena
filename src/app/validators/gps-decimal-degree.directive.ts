import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from "@angular/forms";
import { IsNumericValidatorDirective } from "app/validators/is-numeric-validator.directive";


@Directive({
  selector: '[gpsDecimalDegree]',
  providers: [{provide: NG_VALIDATORS, useExisting: GPSDecimalDegreeDirective, multi: true}]
})

export class GPSDecimalDegreeDirective extends IsNumericValidatorDirective implements Validator{

    validate(control: AbstractControl): {[key: string]: any} {
      // replace ',' with '.' and check if value is numeric 
      let result = super.validate(control);

      if(result == null){
        if(control.value < -180 || control.value > 180){
            result = { outOfRange: true };
        }
      }
      return result;
    }

}