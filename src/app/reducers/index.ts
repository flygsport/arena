import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer,
  Action
} from '@ngrx/store';
import { routerReducer } from '@ngrx/router-store';
import { storeFreeze } from 'ngrx-store-freeze';
import { environment } from '../../environments/environment';
import { clubReducer } from './club.reducers';

export interface AppState {
  clubs;
}

export const reducers: ActionReducerMap<AppState> = <ActionReducerMap<AppState, Action>>{
  router: routerReducer,
  clubs: clubReducer,
};


export const metaReducers: MetaReducer<AppState>[] = !environment.production
  ? [storeFreeze]
  : [];

