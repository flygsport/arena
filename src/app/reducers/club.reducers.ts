import { Action, createFeatureSelector, createSelector } from '@ngrx/store';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { ClubActionTypes, LoadClubsSuccess, CreateClubSuccess, UpdateClubSuccess } from '../actions/club.actions';
import { Club } from '../services/fsf.service.types';

export interface ClubState extends EntityState<Club> { }
const clubAdapter = createEntityAdapter<Club>();

const initialClubState: ClubState = clubAdapter.getInitialState();

export function clubReducer(state: ClubState = initialClubState, action: Action): ClubState {
  switch (action.type) {
    case ClubActionTypes.LOAD_CLUBS_SUCCESS:
      return clubAdapter.addAll((action as LoadClubsSuccess).payload, state);
    case ClubActionTypes.CREATE_CLUB_SUCCESS:
      return clubAdapter.addOne((action as CreateClubSuccess).payload, state);
    case ClubActionTypes.UPDATE_CLUB_SUCCESS:
      const club = (action as UpdateClubSuccess).payload;
      return clubAdapter.updateOne({ id: club.id, changes: club}, state);
    default:
      return state;
  }
}

export const {
  selectIds: selectClubIds,
  selectEntities: selectClubEntities,
  selectAll: selectAllClubs,
  selectTotal: clubCount
} = clubAdapter.getSelectors();

const getClubState = createFeatureSelector<ClubState>('clubs');
