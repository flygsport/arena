import { AfterViewInit, Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { Arena, FSFService } from '../services/fsf.service';
import { arenaLanguageMapper, arenaSeMap } from '../services/fsf.service.types';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs';

import { Club } from '../services/fsf.service.types';
import { ClubStore } from '../services/club.service';
import { ArenaFilterStore } from '../services/arena-filter.service';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { ExportExelDialogComponent } from '../export-exel-dialog/export-exel-dialog.component';

/**
 * Extend MatTableDataSource to extend the filter function with arena specific attributes.
 */
export class ArenaTableDataSource extends MatTableDataSource<Arena> {
  private readonly EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
  private filterValue;

  constructor(dataSource: Arena[]) {
    super(dataSource);
    this.filterPredicate = this.filterMethod;
  }

  setFilterValue(val) {
    this.filterValue = val;
    this.filter = val;
  }

  filterMethod(arena: Arena, ignore: string) {
    // ID
    // NOTE, compare using != for automatic type conversion
    if (this.filterValue.fsfID && (arena.id != this.filterValue.fsfID)) {
      return false;
    }
    // arena name
    if (this.filterValue.arenaName && !(this.stringMatch(arena.name, this.filterValue.arenaName) ||
      this.stringMatch(arena.name2, this.filterValue.arenaName))) {
      return false;
    }
    if (this.filterValue.arenaType && this.filterValue.arenaType !== arena.kind) {
      return false;
    }
    // smffNbr
    if (this.filterValue.smffNbr && !this.stringMatch(arena.smff_nbr, this.filterValue.smffNbr)) {
      return false;
    }
    // icao
    if (this.filterValue.icao && !this.stringMatch(arena.icao, this.filterValue.icao.toLowerCase())) {
      return false;
    }
    // icao, deprecated
    if (!this.booleanMatch(this.filterValue.icaoDeprecated, arena.icao_deprecated)) {
      return false;
    }
    // club
    if (this.filterValue.clubName && !this.clubInClubList(arena.clubList)) {
      return false;
    }
    // Svenska Flygfält
    if (!this.booleanMatch(this.filterValue.officialMapArena, arena.official_map_arena)) {
      return false;
    }
    // --- activity ---
    // take off
    if (!this.booleanMatch(this.filterValue.activity.takeOff, arena.take_off)) {
      return false;
    }
    // landing
    if (!this.booleanMatch(this.filterValue.activity.landing, arena.landing)) {
      return false;
    }
    // in air
    if (!this.booleanMatch(this.filterValue.activity.inAir, arena.in_air)) {
      return false;
    }
    // --- sports ---
    // balooning
    if (!this.booleanMatch(this.filterValue.sport.balooning, arena.balooning)) {
      return false;
    }
    // parachuting
    if (!this.booleanMatch(this.filterValue.sport.parachuting, arena.parachuting)) {
      return false;
    }
    // hanggliding
    if (!this.booleanMatch(this.filterValue.sport.hanggliding, arena.hanggliding)) {
      return false;
    }
    // aerobatic
    if (!this.booleanMatch(this.filterValue.sport.aerobatic, arena.aerobatic)) {
      return false;
    }
    // aeromodeling
    if (!this.booleanMatch(this.filterValue.sport.aeromodeling, arena.aeromodeling)) {
      return false;
    }
    // aviation
    if (!this.booleanMatch(this.filterValue.sport.aviation, arena.aviation)) {
      return false;
    }
    // gliding
    if (!this.booleanMatch(this.filterValue.sport.gliding, arena.gliding)) {
      return false;
    }
    // paragliding
    if (!this.booleanMatch(this.filterValue.sport.paragliding, arena.paragliding)) {
      return false;
    }
    return true;
  }

  /**
   * Checks if this.filterValue.clubName is in the @param activeClubs.
   * @param activeClubs
   */
  private clubInClubList(activeClubs: Club[]): boolean {
    if (activeClubs) {
      const pattern = this.filterValue.clubName.toLowerCase();
      for (let i = 0; i < activeClubs.length; i++) {
        if (this.stringMatch(activeClubs[i].name, pattern)) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Return true if text contains pattern.
   * NOTE!!! @param text is converted to lowercase.
   * @param text - may be undefined, or null
   * @param pattern - the string to search for, must be lower case.
   */
  private stringMatch(text: string, pattern: string): boolean {
    const result = text && pattern && (text.toLowerCase().indexOf(pattern) !== -1);
    return result;
  }

  /**
   * 'do not care'/true/false comparsion
   * Returns false iff a has a boolean value of true/false and b has a different trutfully value
   */
  private booleanMatch(a, b): boolean {
    if (typeof(a) === 'boolean') {
      return a === !!b;
    }
    return true;
  }

  saveAsExcel(fileName: string, map) {
    const filteredData = [];
    this.data.forEach(arena => {
      if (this.filterMethod(arena, null)) {
        filteredData.push(arenaLanguageMapper(arena, map));
      }
    });
    const workbook: XLSX.WorkBook = XLSX.utils.book_new();
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(filteredData);
    XLSX.utils.book_append_sheet(workbook, worksheet, fileName);
    /* save to file */
//    XLSX.writeFile(wb, 'arenor.xlsx');
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    const data: Blob = new Blob([excelBuffer], {type: this.EXCEL_TYPE});
    FileSaver.saveAs(data, fileName);
  }

}

// =============================================================================

@Component({
  selector: 'list-arenas',  // not needed
  templateUrl: './list-arenas.component.html',
  styleUrls: ['./list-arenas.component.scss']
})
export class ListArenasComponent implements OnInit, AfterViewInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('tableWrapper') table: ElementRef;

  displayedColumns = ['id', 'name', 'info', 'icao', 'smff_nbr', 'ts_nbr', 'map'];
  arenaDataSource = new ArenaTableDataSource([]);

  arenaTypes: Observable<string[]>;

  constructor(public router: Router,
    public fsfService: FSFService,
    public dialog: MatDialog,
    public clubStore: ClubStore,
    private filterStore: ArenaFilterStore) {
    this.arenaTypes = fsfService.getArenaTypes();
  }

  ngOnInit() {
    this.fsfService.getAllArenas().subscribe(data => this.arenaDataSource.data = data);

    this.filterStore.arenaFilter$.subscribe(value => this.arenaDataSource.setFilterValue(value));
  }

  ngAfterViewInit() {
    this.arenaDataSource.sort = this.sort;
    this.arenaDataSource.paginator = this.paginator;
  }

  onSelectArena(arena: Arena) {
    this.router.navigate(['/show', arena.id]);
  }

  onViewOnMapArena(arena: Arena) {
    this.router.navigate(['/viewOnMap', arena.id]);
  }

  toInfoString(arena: Arena): string {
    let result = '';
    let sep = '';
    if (arena.kind) {
      result += arena.kind;
      sep = ', ';
    }
    return result;
  }

  openAltNameDialog(event, name): void {
    event.stopPropagation();
    const dialogRef = this.dialog.open(DialogAlternativeNameDialogComponent, {
      width: '250px',
      data: { name: name }
    });
  }

  saveAsExcel() {
//    this.arenaDataSource.saveAsExcel('Arenor');
  const dialogRef = this.dialog.open(ExportExelDialogComponent, {
//      width: '250px',
      data: {dataSource : this.arenaDataSource, nameMap: arenaSeMap }
    });
  }
}

@Component({
  selector: 'dialog-alternative-name-dialog',
  template: `<h1 mat-dialog-title>{{data.name}}</h1>
             <div mat-dialog-content></div>
             <div mat-dialog-actions align='end'>
               <button mat-raised-button color="primary" matDialogClose>ok</button>
             </div>
            `,
})

export class DialogAlternativeNameDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogAlternativeNameDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }
}
