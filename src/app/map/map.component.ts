import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { Router, Params, ActivatedRoute } from "@angular/router";
import Map from 'ol/map';
import View from 'ol/view';

import _ol_tilegrid_WMTS from 'ol/tilegrid/wmts'

import ZoomControl from 'ol/control/zoom';
import ZoomSliderControl from 'ol/control/zoomslider';

import Feature from 'ol/feature';

import Point from 'ol/geom/point';
import Circle from 'ol/geom/circle';

import VectorSource from 'ol/source/vector';
import SourceWMTS from 'ol/source/wmts';
import VectorLayer from 'ol/layer/vector';
import TileLayer from 'ol/layer/tile';

import Style from 'ol/style/style';
import StyleCircle from 'ol/style/circle';
import StyleFill from 'ol/style/fill';
import StyleStroke from 'ol/style/stroke';
import StyleIcon from 'ol/style/icon';

import Projection from 'ol/proj/projection';
import proj from 'ol/proj';

import InteractionSelect from 'ol/interaction/select';

import proj4 from 'proj4';
import { FSFService, Arena } from "../services/fsf.service";
import { mapApiKey } from "../secrets";

/**
 * Sweden center coordinates
 *   609924.45 6877630.37 
 * Projected bounds (SWEREF99 TM):
 *   181896.33 6101648.07
 *   864416.00 7689478.31
 * WGS84 bounds:
 *   10.03 54.96
 *   24.17 69.07
 **/
/**
 * Lantmäteriet "Topografisk Webbkarta Visning" (topowebb) uses the projection SWEREF99 TM (EPSG:3006).
 * https://www.lantmateriet.se/globalassets/kartor-och-geografisk-information/geodatatjanster/tekn_beskrivningar/tb_twkvisningccby_v1.0.1.pdf
 **/



@Component({
  selector: 'air-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  // arena viewed in dialog mode
  @Input() dialogViewArena: Arena;
  // arena selected i map mode
  selectedArena : Arena;
  
  private selectedArenaFeature : Feature;
  private selectedCircleMarker : Feature;
  // Arena id for pre selected arena, i.e. url parameter or @Input() dialogViewArena. 
  // Set by ngAfterViewInit()
  private initialSelectedArenaId;

  private arenaFeatureList: Feature[] = [];
  private sweProjection: Projection;  // SWEREF99

  // Map topoweb source details
  private extent = [ -1200000, 4700000, 2600000, 8500000 ];
  private resolutions = [ 4096.0, 2048.0, 1024.0, 512.0, 256.0, 128.0, 64.0, 32.0, 16.0, 8.0 ];
  private matrixIds = [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ];


  constructor(private router: Router,     
              private route: ActivatedRoute,
              private fsfService: FSFService) { 
      // create a projection for SWEREF99
      proj.setProj4(proj4);
      proj4.defs('EPSG:3006', '+proj=utm +zone=33 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs');
      this.sweProjection = new Projection({
          code: 'EPSG:3006',
          // The extent is used to determine zoom level 0. Recommended values for a
          // projection's validity extent can be found at https://epsg.io/.
          //extent: [485869.5728, 76443.1884, 837076.5648, 299941.7864],
          units: 'm'
        });
        proj.addProjection(this.sweProjection);
  }

  onGotoArena() {
    if(this.selectedArena){
      this.router.navigate(['/show', this.selectedArena.id]);
    }
  }

  ngOnInit() {
  }
  
  ngAfterViewInit(){
    // select arena using inpout parameter (dialog on arena ditails page)
    if(this.dialogViewArena){
      this.initialSelectedArenaId = this.dialogViewArena.id;
    } else {
      // select arena by router parameter (url naviagation)
      this.route.params.subscribe((params: Params) => { 
          this.initialSelectedArenaId = +params['id'];  // convert to number
      })
    }

      //--- lantmäteriet map layer ---
      const tileGrid = new _ol_tilegrid_WMTS({
        tileSize : 256,
        extent : this.extent,
        resolutions : this.resolutions,
        matrixIds : this.matrixIds
      });

      const wmtsMapLayer = new TileLayer({
        extent : this.extent,
        source : new SourceWMTS({
          url : 'https://api.lantmateriet.se/open/topowebb-ccby/v1/wmts/token/' + mapApiKey + '/',
          layer : 'topowebb',
          format : 'image/png',
          matrixSet : '3006',
          tileGrid : tileGrid,
          version : '1.0.1',
          style : 'default',
          crossOrigin: 'anonymous'
        })
      });

      //--- selected arena layer ---------------------------------
      const selectCircleStyle= new Style({
          // inner circle
          image: new StyleCircle({
            radius: 48,
            fill: new StyleFill({
                color: 'rgba(255, 255, 255, 0.4)'
            }),
          // border
          stroke: new StyleStroke({
              color: 'rgba(0, 0, 0, 0.7)', //'#ffcc33',
              width: 5
            }),
          })
        });
      const mapGreenPinStyle = new Style({
          image: new StyleIcon({
            size: [48, 48],
            anchor: [0.5, 0],
            anchorOrigin: 'bottom-left',
            anchorXUnits: 'fraction',
            anchorYUnits: 'pixels',
            src: 'assets/map-pin-green.png'
          })
        });

      this.selectedCircleMarker = new Feature({
          geometry: new Point([ 0, 0 ], 'EPSG:4326', 'EPSG:3006')
        });

      var selectSource = new VectorSource({
          features: [ this.selectedCircleMarker ]
        });

      var selectLayer = new VectorLayer({
        source: selectSource,
        style: [selectCircleStyle]
      });
    
      //--- arena layer -----------------------------------------------
      const mapRedPinStyle = new Style({
          image: new StyleIcon({
            size: [48, 48],
            anchor: [0.5, 0],
            anchorOrigin: 'bottom-left',
            anchorXUnits: 'fraction',
            anchorYUnits: 'pixels',
            src: 'assets/map-pin-red.png'
          })
        });

      // --- fetch all arenas and create the arena layer
      this.fsfService.getAllArenas().subscribe(data => {
          // -- fetch all arenas and make a feauture for each
          var mapCenter = [ 609924.45, 6877630.37 ];
          var zoom = 0.6;
          data.forEach(a => {
            // skipp the arena in @Input() dialogViewArena. It is added later.
            if(!this.dialogViewArena || this.dialogViewArena.id !== a.id){
              // make a feature of the arena and add it to the list 
              const pos = proj.transform([ a.latitude, a.longitude ], 'EPSG:4326', 'EPSG:3006');
              const arenaFeature = new Feature({
                geometry: new Point(pos),
                arena: a
              });
              this.arenaFeatureList.push(arenaFeature);
              // if selected, center map on arena
              if(this.initialSelectedArenaId === a.id){ 
                this.selectedArena = a;
                this.selectedArenaFeature = arenaFeature;
                this.selectedCircleMarker.setGeometry(new Point(pos));
                mapCenter = pos;
                zoom = 4;
              }
            }
          });  // end data.foreach(...)
          //--- dialog mode, add the arena in @Input() dialogViewArena.
          if(this.dialogViewArena ){
            // make a feature of the arena
            let a  = this.dialogViewArena;
            const pos = proj.transform([ a.latitude, a.longitude ], 'EPSG:4326', 'EPSG:3006');
            const arenaFeature = new Feature({
              geometry: new Point(pos),
              arena: a
            });
            // center map on arena
            this.selectedArena = a;
            this.selectedArenaFeature = arenaFeature;
            this.selectedCircleMarker.setGeometry(new Point(pos));
            mapCenter = pos;
            zoom = 4;
          }
          
          var arenaSource = new VectorSource({
              features: this.arenaFeatureList
            });
          var arenaLayer = new VectorLayer({
              source: arenaSource,
              style: [mapRedPinStyle]
            });

          // --- create the map view opbects and attach it to the DOM -------------------
          var map = new Map({
              target : 'map',
              layers : [ wmtsMapLayer, selectLayer, arenaLayer ],
              logo : false,
              controls : [new ZoomControl(), new ZoomSliderControl()],
              view : new View({
                projection: 'EPSG:3006',
                extent : this.extent,
                center : mapCenter, // lantmäteriets center: 616542, 6727536 ],
                zoom : zoom,
                resolutions : this.resolutions
              })
            });

          // --- add functionallity for highlighting selected arena, and select by clicking  --------
          var selectSingleClick = new InteractionSelect({ style: mapGreenPinStyle, layers: [ arenaLayer ] });
          if(this.selectedArenaFeature){
            selectSingleClick.getFeatures().push(this.selectedArenaFeature);
          }
          selectSingleClick.on('select', event => {
            // do not select other arenas in dialog mode
            if(!this.dialogViewArena){
              if( event.selected.length > 0 ){
                // one arena selected
                const feature = event.selected[0];
                this.selectedArenaFeature = feature;
                const arena = feature.get('arena');
                this.selectedArena = arena;
                this.selectedCircleMarker.setGeometry(feature.getGeometry());
              } else {
                // no arena selected
                this.selectedArena = null;
                this.selectedArenaFeature = null;
                this.selectedCircleMarker.setGeometry(new Point([ 0, 0 ], 'EPSG:4326', 'EPSG:3006'));
              }
            }
          }, this);
          map.addInteraction(selectSingleClick);

    }); // end getAllArenas()
  }
}
