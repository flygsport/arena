import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

import { Arena } from "../services/fsf.service";

@Component({
  selector: 'dialog-map-dialog',
  templateUrl: './map.dialog.component.html',
  styleUrls: ['./map.dialog.component.scss']
})
export class DialogMapDialog {

  constructor(
    public dialogRef: MatDialogRef<DialogMapDialog>,
    @Inject(MAT_DIALOG_DATA) public arena: Arena
  ) { }
  
  closeDialog(){
    this.dialogRef.close();
  }

}
