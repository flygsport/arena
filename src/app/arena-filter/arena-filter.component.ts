import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';

import { Club } from '../services/fsf.service.types';
import { ArenaFilterStore } from '../services/arena-filter.service';



@Component({
  selector: 'fsf-arena-filter',
  templateUrl: './arena-filter.component.html',
  styleUrls: ['./arena-filter.component.scss']
})
export class ArenaFilterComponent implements OnInit {
  @Input() clubList: Club[];
  @Input() arenaTypes: string[];

  // th list of clubs apearing as autofill options
  filteredClubList: Club[] = this.clubList;

  public readonly filterForm = this.fb.group({
    arenaName: [''],
    fsfID: [''],
    arenaType: [null],
    smffNbr: [''],
    icao: [''],
    icaoDeprecated: [false],
    clubName: [''],
    officialMapArena: [false],
    activity: this.fb.group({
      takeOff: false,
      landing: false,
      inAir: false
    }),
    sport: this.fb.group({
      balooning: false,
      parachuting: false,
      hanggliding: false,
      aerobatic: false,
      aeromodeling: false,
      aviation: false,
      gliding: false,
      paragliding: false,
    })
  });

  // Is the filter mat-expansion-panel opened?
  panelOpenState: boolean;

  constructor(private fb: FormBuilder, private store: ArenaFilterStore ) {
    try {
      this.filterForm.setValue(this.store.getFilter());
    } catch (e) {
      console.log('failed to set filter values from store');
    }
  }

  ngOnInit() {
    this.panelOpenState = !this.store.isDefaultFilter();
    this.filterForm.valueChanges.subscribe(value => this.store.setFilter(value));
    this.filterForm.get('clubName').valueChanges.subscribe(pattern => {
        pattern = pattern.toUpperCase();
        this.filteredClubList = this.clubList.filter((club) => {
          if (pattern) {
            return club.name.toUpperCase().indexOf(pattern) !== -1;
          } else {
            return true;
          }
        });
    });
  }

  ngAfterInit() {
    this.filteredClubList = this.clubList;
  }

  /**
   * Make a checkbox cycle between the values null/true/false
   * @param name - form filed name
   */
  toggleCheckbox(name: string): boolean {
    if (this.filterForm.value[name] === false) {
      const tmp = {};
      tmp[name] = null;
      this.filterForm.patchValue(tmp);
      return false; // stop event propagation (else checkbox will get the check box value becomes true)
    }
    return true;
  }
  toggleActivityCheckbox(name: string): boolean {
    if (this.filterForm.value.activity[name] === false) {
      const tmp = {};
      tmp[name] = null;
      this.filterForm.controls.activity.patchValue(tmp);
      return false; // stop event propagation (else checkbox will get the check box value becomes true)
    }
    return true;
  }
  toggleSportCheckbox(name: string): boolean {
    if (this.filterForm.value.sport[name] === false) {
      const tmp = {};
      tmp[name] = null;
      this.filterForm.controls.sport.patchValue(tmp);
      return false; // stop event propagation (else checkbox will get the check box value becomes true)
    }
    return true;
  }
}
