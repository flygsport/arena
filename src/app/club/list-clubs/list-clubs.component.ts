import { Component, OnInit, OnDestroy, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Club } from '../../services/fsf.service.types';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { map } from 'rxjs/operators';
import { selectAllClubs } from '../../reducers/club.reducers';
import { Router } from '@angular/router';

export class ClubTableDataSource extends MatTableDataSource<Club> {
  constructor(dataSource: Club[]) {
    super(dataSource);
  }
}


@Component({
  selector: 'app-list-clubs',
  templateUrl: './list-clubs.component.html',
  styleUrls: ['./list-clubs.component.scss']
})
export class ListClubsComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('tableWrapper') table: ElementRef;

  displayedColumns = ['name', 'name2', 'rf_nr', 'kommun'];
  clubsDataSource = new ClubTableDataSource([]);
  private clubSubscription;

  constructor(
    private store: Store<any>,
    public router: Router,
  ) { }

  ngOnInit() {
    const clubs$: Observable<Club[]> = this.store.pipe(select('clubs')).pipe(map(s => selectAllClubs(s)));
    this.clubSubscription = clubs$.subscribe( (clubList: Club[]) => {
      this.clubsDataSource.data = clubList;
    });
  }

  ngAfterViewInit() {
    this.clubsDataSource.sort = this.sort;
    this.clubsDataSource.paginator = this.paginator;
  }

  ngOnDestroy(): void {
    if (this.clubSubscription) {
      this.clubSubscription.unsubscribe();
      this.clubSubscription = null;
    }
  }

  onSelectClub(club: Club) {
    this.router.navigate(['/club', club.id]);
  }
}
