import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store, select } from '@ngrx/store';
import { CreateClubRequest, CreateClubSuccess, ClubActionTypes, UpdateClubRequest } from '../../actions/club.actions';
import { Actions, ofType } from '@ngrx/effects';
import { exhaustMap, tap, switchMap, map, filter } from 'rxjs/operators';
import { userSelector } from '@holmby/auth';
import { ActivatedRoute, Params } from '@angular/router';
import { Club } from '../../services/fsf.service.types';
import { selectClubEntities } from '../../reducers/club.reducers';
import { Observable } from 'rxjs';
import { CloseScrollStrategy } from '@angular/cdk/overlay';

enum FormState {EDIT, PROCESSING, UPDATED, CREATED}

@Component({
  selector: 'app-club-form',
  templateUrl: './club-form.component.html',
  styleUrls: ['./club-form.component.scss']
})
export class ClubFormComponent implements OnInit, OnDestroy {
  FormState = FormState;  // make enum values visible in html template
  formState = FormState.EDIT;

  clubFormGroup: FormGroup = this.fb.group({
    id: [''],
    name: ['', Validators.required],
    name2: ['', Validators.required],
    sf: ['Svenska Flygsportförbundet'],
    org_nr: [''],
    rf_nr: [''],
    kommun: [''],
    epost: [''],
    hemsida: [''],
    active: [true],
  });

  private actionSubscription;

  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    public store: Store<any>,
    public actions$: Actions
  ) { }

  ngOnInit() {
    this.actionSubscription = this.actions$.pipe(
      tap(action => {
        if (action.type === ClubActionTypes.UPDATE_CLUB_SUCCESS) {
          this.formState = FormState.UPDATED;
        } else if (action.type === ClubActionTypes.CREATE_CLUB_SUCCESS) {
          this.formState = FormState.CREATED;
        }
      })
    ).subscribe();

    // initialise form if a club id is provided
    const clubs$ = this.store.pipe(select('clubs')).pipe(map(s => selectClubEntities(s)));
    this.route.params.pipe(
      filter(params => params['id']), // create new club
      switchMap((params: Params) => clubs$.pipe(map(clubs => clubs[params['id']]))),
      filter(club => !!club), // club === undefined befor the list is fetched from the server
    ).subscribe(club => this.clubFormGroup.patchValue(club));
  }

  ngOnDestroy(): void {
    if (this.actionSubscription) {
      this.actionSubscription.unsubscribe();
      this.actionSubscription = null;
    }
  }

  getSubitActionText() {
    switch (this.formState) {
    case FormState.EDIT:
      if (this.clubFormGroup.value.id) {
        return 'uppdatera klubbinformatiomnen';
      } else {
        return 'skapa ny klubb';
      }
      break;
    case FormState.PROCESSING:
      return 'bearbetar';
    case FormState.UPDATED:
      return 'klar, klubbinformationen finns nu i databasen';
    default:
      return 'internt fel';
    }
  }

  onSubmit() {
    if (this.clubFormGroup.valid) {
      if (this.formState === FormState.EDIT) {
        this.formState = FormState.PROCESSING;
        if (this.clubFormGroup.value.id) {
          this.store.dispatch(new UpdateClubRequest(this.clubFormGroup.value));
        } else {
          this.store.dispatch(new CreateClubRequest(this.clubFormGroup.value));
        }
      }
    } else {
      // Requered filds not filled, mat-error is shown automatically
    }
  }
}
