import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatPaginatorModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule,
  } from '@angular/material';

import { ListClubsComponent } from './list-clubs/list-clubs.component';
import { ViewClubComponent } from './view-club/view-club.component';
import { ClubFormComponent } from './club-form/club-form.component';

  @NgModule({
  declarations: [
    ListClubsComponent,
    ViewClubComponent,
    ClubFormComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,

    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatPaginatorModule,
    MatSelectModule,
    MatSortModule,
    MatTableModule,

  ]
})
export class ClubModule { }
