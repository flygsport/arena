import { Injectable } from '@angular/core';
import { exhaustMap, map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { FSFService } from './services/fsf.service';
import {
  ClubActionTypes, CreateClubRequest, CreateClubFail, CreateClubSuccess,
  UpdateClubRequest, UpdateClubSuccess, UpdateClubFail
 } from './actions/club.actions';

@Injectable()
export class AppEffects {

  // --- Clubs ---
  @Effect()
  createClub$ = this.actions$.pipe(
    ofType<CreateClubRequest>(ClubActionTypes.CREATE_CLUB_REQUEST),
    exhaustMap(action =>
      this.fsfService.createClub(action.payload).pipe(
        map(response => new CreateClubSuccess(response[0]) ),
        catchError(error => of(new CreateClubFail(error)))
      )
    )
  );

  @Effect()
  updateClub$ = this.actions$.pipe(
    ofType<UpdateClubRequest>(ClubActionTypes.UPDATE_CLUB_REQUEST),
    exhaustMap(action =>
      this.fsfService.updateClub(action.payload).pipe(
        map(response => new UpdateClubSuccess(response[0]) ),
        catchError(error => of(new UpdateClubFail(error)))
      )
    )
  );

  constructor(
    private actions$: Actions,
    private fsfService: FSFService,
  ) { }
}
