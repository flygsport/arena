import { identifierModuleUrl } from '@angular/compiler';
import { map } from 'rxjs/operators';

export interface Arena {
  id: number;
  name: string;
  name2: string;
  kind: string;

  longitude: number;
  latitude: number;
  altitude_ground: number;
  radius: number;
  height: number;
  official_map_arena: boolean;

  aerobaticBoxBottom: number;
  aerobaticBoxTop: number;

  url: string;
  email: string;
  radio: number;
  phone: string;

  icao: string;
  icao_deprecated: boolean;
  smff_nbr: string;
  ts_nbr: string;

  landing: boolean;
  take_off: boolean;
  in_air: boolean;

  aerobatic: boolean;
  aeromodeling: boolean;
  aviation: boolean;
  balooning: boolean;
  gliding: boolean;
  hanggliding: boolean;
  parachuting: boolean;
  paragliding: boolean;

  comment: string;
  local_regulations: string;

  clubList: Club[];
}


/**
 * Mapping from Arena interface parameter names to swedish names.
 */
export const arenaSeMap = [
  {src: 'id', dst: 'FSF-ID'},
  {src: 'name', dst: 'namn'},
  {src: 'name2', dst: 'alternativt namn'},
  {src: 'kind', dst: 'typ'},

  {src: 'longitude', dst: 'longitud'},
  {src: 'latitude', dst: 'latitud'},
  {src: 'altitude_ground', dst: 'markhöjd'},
  {src: 'radius', dst: 'radie'},
  {src: 'height', dst: 'höjd'},
  {src: 'official_map_arena', dst: 'svenska flygfält'},

  {src: 'aerobaticBoxBottom', dst: 'konstflygbox, undersida'},
  {src: 'aerobaticBoxTop', dst: 'konstflygbox, översida'},

  {src: 'icao', dst: 'ICAO'},
  {src: 'icao_deprecated', dst: 'ICAO, nerlagd'},
  {src: 'smff_nbr', dst: 'modellflygplatsnummer'},
  {src: 'ts_nbr', dst: 'TS nummer'},

  {src: 'url', dst: 'url'},
  {src: 'email', dst: 'email'},
  {src: 'radio', dst: 'radie'},
  {src: 'phone', dst: 'telefon'},

  {src: 'landing', dst: 'landningsplats'},
  {src: 'take_off', dst: 'startplats'},
  {src: 'in_air', dst: 'i ludften aktivitet'},

  {src: 'aerobatic', dst: 'konstflyg'},
  {src: 'aeromodeling', dst: 'modellflyg'},
  {src: 'aviation', dst: 'motorflyg'},
  {src: 'balooning', dst: 'balong'},
  {src: 'gliding', dst: 'segel'},
  {src: 'hanggliding', dst: 'hängflyg'},
  {src: 'parachuting', dst: 'fallskärm'},
  {src: 'paragliding', dst: 'skärmflyg'},

  {src: 'comment', dst: 'kommentar'},
  {src: 'local_regulations', dst: 'lokala regler'}
];

/**
 *
 * @param arena Returns an arena object which properties have swedish names.
 * Used when exporting to exel.
 */
export const arenaLanguageMapper = function(arena: Arena, arenaMap: [{src: string, dst: string}]) {
  const result = {};
  arenaMap.forEach(mapping => {
    result[mapping.dst] = arena[mapping.src];
  });
  return result;
};

export interface Club {
    id: number;
    name: string;
    name2: string;
    sf: string;
    df: string;
    org_nr: string;
    rf_nr: number;
    kommun: string;
    epost: string;
    hemsida: string;
    active: boolean;
}
