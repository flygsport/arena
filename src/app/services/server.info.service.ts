
import {of as observableOf,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";



import { FetchJSON } from './json';

export interface ServerInfo {
  maxFileSize: number;
  productionServer: boolean;
  name: string;
}

@Injectable()
export class ServerInfoService extends FetchJSON {
  private _server = null;

  constructor(http: HttpClient) { super(http); }

  public getServerInfo(): Observable<ServerInfo> {
    return this.get('rest/ServerInfo.php');
  }

}
