
import {from as observableFrom,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';


import { FetchJSON } from './json';
import { Arena, Club } from './fsf.service.types';

export { Arena };


@Injectable()
export class FSFService extends FetchJSON {
  private readonly arenaTypes = observableFrom([['Flygplats',
                                 'Flygfält',
                                 'Inomhusflyg, Modellflyg',
                                 'Konstflygbox',
                                 'Modellflygfält',
                                 'Startplats Ballong',
                                 'Startplats Häng- och Skärmflyg',
                                 'Vindtunnel',
                                 'Vinchplats',
                                 'Annat' ]]);


  constructor(http: HttpClient) { super(http); }

  public getArenaTypes(): Observable<string[]> {
    return this.arenaTypes;
}

  /**
   *  Returns one arena.
   */
  public getArena(id): Observable<Arena[]> {
    const params: HttpParams = new HttpParams().set('id', id) ;
    return this.get('rest/arena.php', params);
  }

  /**
   *  Returns all arenas
   */
  public getAllArenas(): Observable<Arena[]> {
    return this.get('rest/arena.php');
  }

  /**
   * Create a new air sport location.
   * return: the created air sport location
   */
  public createArena(arena: Arena): Observable<Arena[]> {
    return this.post('rest/arena.php', arena);
  }

  /**
   * Create a new air sport location.
   * return: the created air sport location
   */
  public updateArena(arena: Arena): Observable<Arena[]> {
    return this.put('rest/arena.php', arena);
  }

  public getAllClubs(): Observable<Club[]> {
      return this.get('rest/club.php');
  }

  public createClub(club: Club) {
    return this.post<Club>('rest/club.php', club);
  }

  public updateClub(club: Club): Observable<Club[]> {
    return this.put('rest/club.php', club);
  }

  public getAttachmentList(arenaId): Observable<any[]> {
    const params: HttpParams = new HttpParams().set('arena_id', arenaId) ;
    return this.get('rest/attachment.php', params);
  }

}
