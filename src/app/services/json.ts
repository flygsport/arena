import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from "@angular/common/http";
//import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

/**
 *  FetchJSON is a base class for calling RESTful operations on a remote server.
 *  I provides error handling, infoming the user if an error occurs.
 *  When an error occurs, an alert is shown to inform the user.
 *
 *  Call the get(), post()... methods to make a call to the server.
 *  The methods returns an Observable with with the server respons json object (reply.data).
 *
 *  For error handling in the caller, subsribe to Observable error channel (onError).
 *  When an error occurs the error object will be passed to the error channel.
 *
 *  The response from the server must contain one object with a field named success.
 *  When success === true, the reply object type is:
 *    {
 *      success:boolean,
 *      data:any
 *     }
 *  When success === false, the reply object type is:
 *    {
 *      success:boolean,
 *      userErrorMessageL string,
 *      developerErrorMessage: string,
 *      stackTrace: string
 *      url: string
 *    }
 */
export class FetchJSON {
  /**
   *  Removes all html tags from a string.
   *  returns: a string with no html tags.
   */
  public static stripHTML(html: string): string {
    let div = document.createElement('div');
    div.innerHTML = html;
    return div.textContent || div.innerText || '';
  }

  // return an object containing the JSON of the response
  protected static extractData(response: Response): any {
    // Check if the http response content type is JSON.
    if (response.headers.get('Content-Type') &&
        response.headers.get('Content-Type').toLowerCase().indexOf('application/json') >= 0) {
      try {
        let data = response.json();
        if (data['success']) {
          return data['data'];
        } else {
          // Error message returned, pass the http response down the error stream,
          // i.e. handleError()
          throw response;
        }
      } catch (e) {
        console.log ('SAM internal error in FetchJSON.extractData, exception parsing JSON.\n'
        + 'exception: ' + e
        + 'body: ' + response.text());
        alert ('SAM internal error in FetchJSON.extractData, exception parsing JSON: '
               + response.text());
        throw response;
      }
      // an error occurred
    } else {
      // If not JSON, pass the HTTP response down the error stream, i.e. handleError()
      throw response;
    }
  }

  protected static handleError<T>(error: Response): Observable<T> {
    // check if we have a JSON reply containing error messages
    if (error.headers.get('Content-Type') &&
        error.headers.get('Content-Type').toLowerCase().indexOf('application/json') >= 0) {
         this.printSamJsonError(error);
    } else {
      // No error messages from the server side
      FetchJSON.handleUnknownHttpError(error);
    }
    throw error;
  }

  /**
   *  Print error messages
   *  Response must contain a JSON encoded reply from the server side
   */
  protected static printSamJsonError(response: Response) {
    try {
      let error = response.json();
      if (error['success']) {
        // This happens if there is an exception in the JavaScript when handling a successful reply,
        // i.e. extactData() thrown an exception.
        console.log ('SAM internal error in FetchJSON.handleError, '
                     + 'A successfyll request was passed down the error chain. '
                     + 'See earlier error messages for the root cause.');
        alert ('SAM internal error in FetchJSON.handleError()');
      } else {
        console.log ('SAM server side error.\n'
          + 'url: ' + error['url']
          + '\n http status: ' + response.status
          + '\n http status text: ' + response.statusText
          + '\nuser message: ' + error['userErrorMessage']
          + '\ndeveloper message: ' + error['developerErrorMessage']
          + '\nstack trace: ' + error['stackTrace']);
        alert (error['userErrorMessage'] || 'unknown server side error');
      }
    } catch (e) {
      console.log ('SAM internal error in FetchJSON.printSamError(), exception parsing JSON.\n'
      + 'exceptin: ' + e
      + 'body: ' + response.text());
      alert ('SAM internal error in FetchJSON.printSamError(), exception parsing JSON response.');
    }
  }

  /**
   *  handleUnknownHttpError() is called when a http request do not contain a JSON body.
   */
   protected static handleUnknownHttpError(response: Response) {
     //  console.error(response.toString());
     let msg = 'http request for ' + response.url + ' did not return a JSON object.'
             + '\n status: ' + response.status
             + '\n status text: ' + response.statusText;
//             + '\n body: ' + FetchJSON.stripHTML(response.text);
     console.error(msg);
     alert('Internal Error. The server returned http error ' + response.status);
   }

  constructor(private http: HttpClient) {  }

  /**
   *  Send a GET request for url.
   *  url - a base url address, i.e. "http://localhost/portal/rest/course.php"
   *  parameters - will be added as the parameter part of the url, i.e. "?user=dat12vat"
   */
  protected get<T>(url: string, parameters ?: HttpParams): Observable<T> {
    const options = parameters ? { params: parameters } : {};
    return this.http.get<T>(url, options)
        .pipe(map((response) => { return response['data']; }),
              catchError((error) => { return FetchJSON.handleError<T>(error); }));
  }

  /**
   *  Send a POST request for URL. Commonly used to create a new resource.
   *  @param url - a base URL address, i.e. "http://localhost/portal/rest/course.php"
   *  @param body - will be send as json text in the body of the request.
   */
  protected post<T>(url: string, body): Observable<T> {
    return this.http.post<T>(url, body)
        .pipe(map((response) => { return response['data']; }),
              catchError((error) => { return FetchJSON.handleError<T>(error); }));
  }

  /**
   *  Send a PUT request for URL. Commonly used to update a resource.
   *  @param url - a base URL address, i.e. "http://localhost/portal/rest/course.php"
   *  @param body - will be send as json text in the body of the request.
   */
  protected put<T>(url: string, body): Observable<T> {
    return this.http.put<T>(url, body)
        .pipe(map((response) => { return response['data']; }),
              catchError((error) => { return FetchJSON.handleError<T>(error); }));
  }
}
