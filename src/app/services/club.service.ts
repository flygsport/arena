import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { BehaviorSubject } from 'rxjs';
import { Club } from './fsf.service.types';
import { FetchJSON } from './json';

@Injectable({
  providedIn: 'root'
})

export class ClubStore extends FetchJSON {

  private _dataSource = new BehaviorSubject<Club[]>([]);
  public readonly clubList$ = this._dataSource.asObservable();

  constructor(http: HttpClient) {
    super(http);
    this.get<Club[]>('/rest/fsf/v1/club.php').subscribe(result => this._dataSource.next(result));
  }

}
