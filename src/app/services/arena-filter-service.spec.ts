import { TestBed } from '@angular/core/testing';

import { ArenaFilterStoreService } from './arena-filter-store.service';

describe('ArenaFilterStoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ArenaFilterStoreService = TestBed.get(ArenaFilterStoreService);
    expect(service).toBeTruthy();
  });
});
