import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { FormBuilder } from '@angular/forms';

export interface ArenaFilter {
  arenaName: string;
  fsfID: string;
  arenaType: string;
  smffNbr: string;
  icao: string;
  icaoDeprecated: boolean;
  clubName: string;
  officialMapArena: boolean;
  activity: {
    takeOff: boolean;
    landing: boolean;
    inAir: boolean;
  };
  sport: {
    balooning: boolean;
    parachuting: boolean;
    hanggliding: boolean;
    aerobatic: boolean;
    aeromodeling: boolean;
    aviation: boolean;
    gliding: boolean;
    paragliding: boolean;
  };
}

@Injectable({
  providedIn: 'root'
})
export class ArenaFilterStore {
  readonly defaultFilter = {
      arenaName: '',
      fsfID: '',
      arenaType: '',
      smffNbr: '',
      icao: '',
      icaoDeprecated: null,
      clubName: '',
      officialMapArena: null,
      activity: {
        takeOff: null,
        landing: null,
        inAir: null,
      },
      sport: {
        balooning: null,
        parachuting: null,
        hanggliding: null,
        aerobatic: null,
        aeromodeling: null,
        aviation: null,
        gliding: null,
        paragliding: null,
      }
  };

  private _filterValue: ArenaFilter = this.defaultFilter;
  private _dataSource: BehaviorSubject<ArenaFilter> = new BehaviorSubject<ArenaFilter>(this.defaultFilter);
  public readonly arenaFilter$: Observable<ArenaFilter> = this._dataSource.asObservable();

  constructor() {
  }

  setFilter(filter: ArenaFilter) {
    this._filterValue = filter;
    this._dataSource.next(filter);
  }

  getFilter(): ArenaFilter {
    return this._filterValue;
  }

  isDefaultFilter(): boolean {
    return (this._filterValue.arenaName == this.defaultFilter.arenaName) &&
           (this._filterValue.fsfID == this.defaultFilter.fsfID) &&
           (this._filterValue.arenaType == this.defaultFilter.arenaType) &&
           (this._filterValue.smffNbr == this.defaultFilter.smffNbr) &&
           (this._filterValue.icao == this.defaultFilter.icao) &&
           (this._filterValue.icaoDeprecated == this.defaultFilter.icaoDeprecated) &&
           (this._filterValue.clubName == this.defaultFilter.clubName) &&
           (this._filterValue.officialMapArena == this.defaultFilter.officialMapArena) &&

           (this._filterValue.activity.takeOff == this.defaultFilter.activity.takeOff) &&
           (this._filterValue.activity.landing == this.defaultFilter.activity.landing) &&
           (this._filterValue.activity.inAir == this.defaultFilter.activity.inAir) &&

           (this._filterValue.sport.balooning == this.defaultFilter.sport.balooning) &&
           (this._filterValue.sport.parachuting == this.defaultFilter.sport.parachuting) &&
           (this._filterValue.sport.hanggliding == this.defaultFilter.sport.hanggliding) &&
           (this._filterValue.sport.aerobatic == this.defaultFilter.sport.aerobatic) &&
           (this._filterValue.sport.aeromodeling == this.defaultFilter.sport.aeromodeling) &&
           (this._filterValue.sport.aviation == this.defaultFilter.sport.aviation) &&
           (this._filterValue.sport.gliding == this.defaultFilter.sport.gliding) &&
           (this._filterValue.sport.paragliding == this.defaultFilter.sport.paragliding);
  }

}
