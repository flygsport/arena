import { Component, ViewChild, HostListener, OnInit, OnDestroy } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';

import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';

import { userSelector, AuthUser } from '@holmby/auth';
import { FSFService } from './services/fsf.service';
import { LoadClubsSuccess, LoadClubsFail } from './actions/club.actions';

@Component({
  selector: 'air-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit, OnDestroy {
  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';

  private clubSubscription;

  currentUser$: Observable<AuthUser>;

  constructor(
    private fsfService: FSFService,
    private store: Store<any>) {  }

  ngOnInit() {
    this.currentUser$ = this.store.pipe(select(userSelector));

    this.clubSubscription = this.fsfService.getAllClubs().pipe(
      map(response => new LoadClubsSuccess(response)),
      catchError(error => of(new LoadClubsFail(error)))
    ).subscribe(action => this.store.dispatch(action));

    if (window.innerWidth < 768) {
      this.navMode = 'over';
    }
  }

  ngOnDestroy(): void {
    if (this.clubSubscription) {
      this.clubSubscription.unsubscribe();
      this.clubSubscription = null;
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (event.target.innerWidth < 768) {
      this.navMode = 'over';
      this.sidenav.close();
    }
    if (event.target.innerWidth > 768) {
      this.navMode = 'side';
      this.sidenav.open();
    }
  }
}
