export function humanizeBytes(bytes: number): string {
  if (bytes === 0) {
    return '0 Byte';
  }
  let k = 1024;
  const sizes: string[] = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB'];
  let i: number = Math.floor(Math.log(bytes) / Math.log(k));

  return parseFloat((bytes / Math.pow(k, i)).toFixed(2)) + ' ' + sizes[i] + '/s';
}

export interface Progress {
  loaded: number;
  total: number;
  percent: number;
  speed?: number;
  speedHumanized?: string;
}

export class UploadedFile {
  public file: File;
  public xhr: XMLHttpRequest;
  public json: any;     // the body of the response, with the SAM wrapper stripped away
  public progress: Progress;
  // success: done && !(error || abort)
  public done: boolean;  // true when upload stops (finished, error, or abort)
  public error: boolean;
  public abort: boolean;
  public startTime: number;
  public endTime: number;
  public speedAverage: number;
  public speedAverageHumanized: string;

  constructor(file: File) {
    this.file = file;
    this.xhr = new XMLHttpRequest();
    this.progress = {
      loaded: 0,
      total: 0,
      percent: 0,
      speed: 0,
      speedHumanized: null
    };
    this.done = false;
    this.error = false;
    this.abort = false;
    this.startTime = new Date().getTime();
    this.endTime = 0;
    this.speedAverage = 0;
    this.speedAverageHumanized = null;
  }

  /**
   *  Returns true if the file successfully has been fully uploaded.
   */
  public wasSuccess(): boolean {
    return this.done && !(this.error || this.abort);
  }

  public setJson(json) {
    this.json = json;
  }

  public setProgres(progress: Progress): void {
    this.progress = progress;
  }

  public setError(): void {
    this.error = true;
    this.done = true;
  }

  public setAbort(): void {
    this.abort = true;
    this.done = true;
  }
  
  public resetUploadState(){
      this.done = false;
      this.abort = false;
      this.error = false;
      this.progress = {  
          loaded: 0,
          total: 0,
          percent: 0
      };
  }

  public onFinished(): void {
    this.endTime = new Date().getTime();
    this.speedAverage = this.file.size / (this.endTime - this.startTime) * 1000;
    this.speedAverage = parseInt(<any> this.speedAverage, 10);
    this.speedAverageHumanized = humanizeBytes(this.speedAverage);
    this.done = true;
  }
  
  /**
   * Return the state of the upload as a string, suitable for displaying to the user.
   * The answer is one of: waiting, uploading, done, error, aborted
   */
  public getUploadStateText(): string {
      /**
      let text = {
        0 : 'unsent',
        1 : 'uploading', // 'OPENED',
        2 : 'uploading', // 'HEADERS_RECEIVED',
        3 : 'uploading',
        4 : 'done'
      };
      **/
      let result = '';
      if (this.error) {
        result = 'error';
      } else if (this.abort) {
        result = 'aborted';
      } else if (this.done) {
        result = 'done';
      } else {
        if (this.xhr.readyState > 0) {
          result = 'uploading';
        } else {
          result = 'waiting';
        }
      }
      return result;
    }


}
