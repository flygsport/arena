/**
 *   Based on https://github.com/jkuri/ng2-uploader (MIT license)
 */

import { UploadedFile, humanizeBytes } from './file-upload.types';
import { EventEmitter } from '@angular/core';

export class Ng2Uploader {
  private url: string;
  private cors: boolean = false;
  private withCredentials: boolean = false;
  private multiple: boolean = false;
  private maxUploads: number = 3;
  private data: { [index: string]: any } = {};
  private autoUpload: boolean = true;
  private multipart: boolean = true;
  private method: string = 'POST';
  private debug: boolean = false;
  private customHeaders: any = {};
  private encodeHeaders: boolean = true;
  private authTokenPrefix: string = 'Bearer';
  private authToken: string = undefined;
  private fieldName: string = 'file';
  private previewUrl: boolean = false;
  private calculateSpeed: boolean = false;
  public _emitter: EventEmitter<any> = new EventEmitter();
  public _previewEmitter: EventEmitter<any> = new EventEmitter();

  public setOptions(options: any): void {
    this.url = options.url != null ? options.url : this.url;
    this.cors = options.cors != null ? options.cors : this.cors;
    this.withCredentials = options.withCredentials != null ?
                           options.withCredentials : this.withCredentials;
    this.multiple = options.multiple != null ? options.multiple : this.multiple;
    this.maxUploads = options.maxUploads != null ? options.maxUploads : this.maxUploads;
    this.data = options.data != null ? options.data : this.data;
    this.autoUpload = options.autoUpload != null ? options.autoUpload : this.autoUpload;
    this.multipart = options.multipart != null ? options.multipart : this.multipart;
    this.method = options.method != null ? options.method : this.method;
    this.customHeaders = options.customHeaders != null ? options.customHeaders : this.customHeaders;
    this.encodeHeaders = options.encodeHeaders != null ? options.encodeHeaders : this.encodeHeaders;
    this.authTokenPrefix = options.authTokenPrefix != null ?
                           options.authTokenPrefix : this.authTokenPrefix;
    this.authToken = options.authToken != null ? options.authToken : this.authToken;
    this.fieldName = options.fieldName != null ? options.fieldName : this.fieldName;
    this.previewUrl = options.previewUrl != null ?
                      options.previewUrl : this.previewUrl;
    this.calculateSpeed = options.calculateSpeed != null ?
                          options.calculateSpeed : this.calculateSpeed;
    if (!this.multiple) {
      this.maxUploads = 1;
    }
  }

  public uploadFile(uploadingFile: UploadedFile): void {
    uploadingFile.resetUploadState();
    let xhr = uploadingFile.xhr;
    let form = new FormData();
    form.append(this.fieldName, uploadingFile.file, uploadingFile.file.name);

    Object.keys(this.data).forEach((k) => {
      form.append(k, this.data[k]);
    });

    let time: number = new Date().getTime();
    let load = 0;
    let speed = 0;
    let speedHumanized: string = null;

    xhr.upload.onprogress = (e: ProgressEvent) => {
      if (e.lengthComputable) {
        if (this.calculateSpeed) {
          time = new Date().getTime() - time;
          load = e.loaded - load;
          speed = load / time * 1000;
          speed = parseInt(<any> speed, 10);
          speedHumanized = humanizeBytes(speed);
        }

        let percent = Math.round(e.loaded / e.total * 100);
        if (speed === 0) {
          uploadingFile.setProgres({
            total: e.total,
            loaded: e.loaded,
            percent
          });
        } else {
          uploadingFile.setProgres({
            loaded: e.loaded,
            total: e.total,
            percent,
            speed,
            speedHumanized
          });
        }

        this._emitter.emit(uploadingFile);
      }
    };

    xhr.upload.onabort = (e: Event) => {
      uploadingFile.setAbort();
      this._emitter.emit(uploadingFile);
    };

    xhr.upload.onerror = (e: Event) => {
      uploadingFile.setError();
      this._emitter.emit(uploadingFile);
    };

    xhr.onreadystatechange = () => {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        uploadingFile.onFinished();
        // check the body of the response
        try {
          let data = JSON.parse(xhr.responseText);
          if (data.success) {
            // data: {success:boolean, data:any}
            uploadingFile.setJson(data.data); // put the paypoad in uploadingFile.json
          } else {
              /** data contains:
               * {
               *  userErrorMessage string,
               *  developerErrorMessage: string,
               *  stackTrace: string
               *  url: string
               * }
               */
            uploadingFile.setJson(data);  // keep error message in uploadingFile.json
            uploadingFile.setError();
            console.log ('server side error.\n'
                         + 'url: ' + data.url
                         + '\n http status: ' + xhr.status
                         + '\n http status text: ' + xhr.statusText
                         + '\nuser message: ' + data.userErrorMessage
                         + '\ndeveloper message: ' + data.developerErrorMessage
                         + '\nstack trace: ' + data.stackTrace);
          }
        } catch (e) {
          uploadingFile.setError();
          let developerMessage = 'internal error in ng2-uploader.uploadFile(), '
                               + 'Exception parsing JSON.\n'
                               + 'exception: ' + e
                               + 'body: ' + xhr.responseText;
          uploadingFile.setJson({
                      userErrorMessage: 'internal error in ng2-uploader.uploadFile(), '
                                      + 'Exception parsing JSON.\n',
                      developerErrorMessage: developerMessage,
                      stackTrace: '',
                      url: ''
                      }
                    );
          console.log (developerMessage);
          alert ('internal error in ng2-uploader.uploadFile(), exception parsing JSON: '
                 + xhr.responseText);
        }
        // update status based on http response status code
        if (xhr.status < 200 && xhr.status > 299) {
          uploadingFile.setError();
        }
        this._emitter.emit(uploadingFile);
      }
    };

    xhr.open(this.method, this.url, true);
    xhr.withCredentials = this.withCredentials;

    if (this.customHeaders) {
      Object.keys(this.customHeaders).forEach((key) => {
        xhr.setRequestHeader(key, this.customHeaders[key]);
      });
    }

    if (this.authToken) {
      xhr.setRequestHeader('Authorization', `${this.authTokenPrefix} ${this.authToken}`);
    }

    xhr.send(form);
  }

  public createFileUrl(file: File) {
    let reader: FileReader = new FileReader();
    reader.addEventListener('load', () => {
        this._previewEmitter.emit(reader.result);
    });
    reader.readAsDataURL(file);
  }

  public isFile(file: any): boolean {
    return file !== null && (file instanceof Blob || (file.name && file.size));
  }

  public generateRandomIndex(): string {
    return Math.random().toString(36).substring(7);
  }
}
