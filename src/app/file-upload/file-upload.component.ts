import { Component, NgZone, OnInit, Output, EventEmitter } from '@angular/core';
import { ServerInfo, ServerInfoService } from '../services/server.info.service';

import { Ng2Uploader } from './ng2-uploader';
import { UploadedFile, humanizeBytes } from './file-upload.types';

@Component({
  selector: 'file-upload',
  templateUrl: './file-upload.template.html',
  styles: [`
    .file-upload-delete {
      color:black;
    }

    .file-upload-error {
      color:red;
      font:bold;
    }
    `]
})
export class FileUploadComponent implements OnInit {
  @Output() change = new EventEmitter();
  @Output() error = new EventEmitter();
  @Output() done = new EventEmitter();
  
  private zone: NgZone;
  filesToUpload: UploadedFile[] = [];
  uploading: boolean = false;
  private serverInfo: ServerInfo;

  constructor(private server: ServerInfoService) {
    // TODO this is a race condition, the server must responce before any file upload
    server.getServerInfo().subscribe((data) => {this.serverInfo = data; });
  }

  public ngOnInit() {
    this.zone = new NgZone({ enableLongStackTrace: false });
  }

  /**
   * Upload all files in th equeue.
   * @param parameters
   */
  public uploadAllFiles(parameters) {
    this.uploading = true;
    let i = 0;
    let uploader = new Ng2Uploader();
    let options = {
      url: 'rest/attachment.php',
      data: parameters
    };
    uploader.setOptions(options);
    uploader._emitter.subscribe((uploadFile) => {
        if( uploadFile.error || uploadFile.abort){
            // show edit mode
            this.uploading = false;
            this.error.emit(uploadFile);
        }
        let allDone = true;
        for (i = 0; i < this.filesToUpload.length; i++) {
          allDone == allDone && this.filesToUpload[i].wasSuccess();
        }
        if(allDone){
            this.done.emit(true);
        }
        
    });
    for (i = 0; i < this.filesToUpload.length; i++) {
      let uploadingFile = this.filesToUpload[i];
      if ( ! uploadingFile.wasSuccess()) { // do not upload twice
        uploader.uploadFile(uploadingFile);
      }
    }
  }

  /**
   * Called when the user selects files in the input fiels.
   * @param event
   */
  public onFileSelect(event) {
    this.change.emit({action: "addFiles", event: event});
    let list: File[] = <File[]> event.target.files;
    for (let file of list) {
      let uf = new UploadedFile(file);
      if (file.size > this.serverInfo.maxFileSize) {
        uf.json = {userErrorMessage: 'file to large, max size is ' +
                   this.humanizeBytes(this.serverInfo.maxFileSize)};
        uf.error = true;
      }
      this.filesToUpload.push(uf);
    }
  }

  public removeFile(file: UploadedFile) {
    this.change.emit({action: "removeFile", file: file});
    let index = this.filesToUpload.indexOf(file);
    if (index > -1) {
      this.filesToUpload.splice(index, 1);
    }
  }

  public humanizeBytes(bytes: number): string {
    if (bytes === 0) {
      return '0 Byte';
    }
    let k = 1024;
    const sizes: string[] = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB'];
    let i: number = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(2)) + ' ' + sizes[i];
  }

  public reset() {
    this.uploading = false;
    this.filesToUpload = [];
  }
}
