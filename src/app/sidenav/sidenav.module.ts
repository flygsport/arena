import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MatSidenavModule } from '@angular/material';

import { SidenavComponent } from './sidenav/sidenav.component';
import { SidenavGroupComponent } from './sidenav-group/sidenav-group.component';
import { SidenavItemComponent } from './sidenav-item/sidenav-item.component';

@NgModule({
  imports: [
    CommonModule,
    MatSidenavModule,
    RouterModule
  ],
  declarations: [ SidenavComponent, SidenavGroupComponent, SidenavItemComponent ],
  exports:      [ SidenavComponent, SidenavGroupComponent, SidenavItemComponent ]
})
export class SidenavModule { }
