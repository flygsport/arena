import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';

@Component({
  selector: 'app-sidenav-item',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './sidenav-item.component.html',
  styleUrls: ['./sidenav-item.component.scss']
})
export class SidenavItemComponent implements OnInit {
  @Input() text;
  @Input() link;

  constructor() { }

  ngOnInit() {
  }

}
