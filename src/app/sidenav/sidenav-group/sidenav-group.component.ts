import { Component, Input , OnInit } from '@angular/core';

@Component({
  selector: 'app-sidenav-group',
  templateUrl: './sidenav-group.component.html',
  styleUrls: ['./sidenav-group.component.scss']
})
export class SidenavGroupComponent implements OnInit {
  @Input() title;

  constructor() { }

  ngOnInit() {
  }

}
