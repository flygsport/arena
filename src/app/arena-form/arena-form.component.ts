
import { Component, ViewChild, Inject, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';

import { Observable } from 'rxjs';

import { Store, select } from '@ngrx/store';
import { switchMap, map } from 'rxjs/operators';

import { userSelector, AuthUser } from '@holmby/auth';

import { Arena, FSFService } from '../services/fsf.service';
import { Club } from '../services/fsf.service.types';
import { FileUploadComponent } from '../file-upload/file-upload.component';
import { DialogMapDialog } from '../map/map.dialog.component';
import { selectAllClubs } from '../reducers/club.reducers';


export enum FormState {
    Pristine,
    Unsaved,
    FailedSubmission,
    Saving,
    Saved
}

@Component({
  selector: 'arena-form', // not needed
  templateUrl: './arena-form.component.html',
  styleUrls: ['./arena-form.component.scss']
})

export class ArenaFormComponent implements OnInit, OnDestroy, AfterViewInit {
    [x: string]: any;
  arenaTypes: Observable<string[]>;

  readonly maxLength = { name: 64,
                         url: 128,
                         email: 128,
                         phone: 15,
                         icao: 4,
                         smff_nbr: 8,
                         ts_nbr: 10,
                         comment: 5000,
                         local_regulations: 5000};

  readonly formErrors = {
                     required:   'obligatoriskt',
                     notNumeric: 'ange ett decimaltal',
                     notInteger: 'ange ett heltal',
                     outOfRange: 'utanför intervallet -180° till 180°'
  };

  // Angulars FormControl
  @ViewChild('arenaForm') form;
  @ViewChild('imageUplad') private fileUploadComponent: FileUploadComponent;
  @ViewChild('regulationsFileUplad') private regulationsFileUploadComponent: FileUploadComponent;

  FormState = FormState;            // make the enum values visible in the template
  formState: FormState = FormState.Pristine;
  // the following two states are only used during saving, i.e. this.formState === FormState.Saving
  private arenaSaveState: FormState = FormState.Pristine;   // saving arena state
  private filesSaveState: FormState = FormState.Pristine;   // saving file list state
  private regulationsFilesSaveState: FormState = FormState.Pristine;   // saving file list state

  public arena: Arena = {
          id: null,
          name: null,
          name2: null,
          kind: null,

          longitude: null,
          latitude: null,
          altitude_ground: null,
          radius: null,
          height: null,
          official_map_arena: null,

          aerobaticBoxBottom: null,
          aerobaticBoxTop: null,

          url: null,
          email: null,
          radio: null,
          phone: null,

          icao: null,
          icao_deprecated: false,
          smff_nbr: null,
          ts_nbr: null,

          landing: false,
          take_off: false,
          in_air: false,

          aerobatic: null,
          aeromodeling: null,
          aviation: null,
          balooning: null,
          gliding: null,
          hanggliding: null,
          parachuting: null,
          paragliding: null,

          comment: null,
          local_regulations: null,

          clubList: []

};

  // this object encapsulate all data connected to the club tab.
  clubInput = {
       filter: '',
       filteredList: [],  // all clubs matching the text written in the club input field
       allList: []        // the list of clubs fetched from the server
  };

  // --- images ---
  imageList = [];
  files = {};
  // The time to show the next photo
  NextPhotoInterval = 5000;
  // Looping or not
  noLoopSlides = true;

  currentUser$: Observable<AuthUser>;
  currentUser: AuthUser;
  currentUserSubscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fsfService: FSFService,
    private dialog: MatDialog,
    private store: Store<any>,
    ) {
    this.arenaTypes = fsfService.getArenaTypes();
  }

  ngOnInit() {
    this.currentUser$ = this.store.pipe(select(userSelector));
    this.currentUserSubscription = this.currentUser$.subscribe(user => this.currentUser = user);

    // load arena info from server, if an id is provided in the url
    this.route.params.pipe(switchMap((params: Params) => {
      // check if an arena id is provided
      if (!params['id']) { return []; }
      return this.fsfService.getArena(+params['id']);
    }))
    .subscribe((arena: Arena[]) => { this.resetArenaForm(arena[0]); });

    // fetch the list of all clubs
    const clubs$: Observable<Club[]> = this.store.pipe(select('clubs')).pipe(map(s => selectAllClubs(s)));

    clubs$.subscribe( (clubList: Club[]) => {
      this.clubInput.allList = clubList;
      this.clubInput.filteredList = this.filterClubs(this.clubInput.filter);
    });
  }
  ngOnDestroy(): void {
    if (this.currentUserSubscription) {
      this.currentUserSubscription.unsubscribe();
    }
  }

  ngAfterViewInit() {
    // register a form change observer
    this.form.control.valueChanges.subscribe(v => {
      this.formInputChange();
      // update the list of matching clubs
      if (this.clubInput.allList) {
        this.clubInput.filteredList = this.filterClubs(v.club);
      }
    });
  }

  private filterClubs(filter: String): Club[] {
    if (filter) {
      filter = filter.toUpperCase();
      return this.clubInput.allList.filter((club) => {
        return club.name.toUpperCase().indexOf(filter) !== -1;
      });
    } else {
      return this.clubInput.allList;
    }
  }

  /**
  * Return the error message associated with the validation error object
  * @param obj
  */
  public getErrorText(obj: {[key: string]: any} ) {
    if (obj) {
      for (const key in obj) {
        return this.formErrors[key];
      }
    }
  }

  /**
   * Return the save/update label fo rthe submitt button
   */
  submittButtonText() {
      return this.arena.id ? 'Uppdatera arenan' : 'Registrera ny arena';
  }

  /**
   * Add one club to the list of clubs active at the arena.
   */
  public addClub() {
      const clubName = this.clubInput.filter;
      let i = 0;
      while (i < this.clubInput.allList.length && this.clubInput.allList[i].name !== clubName) {
        i++;
      }
      if (i < this.clubInput.allList.length) {
        this.arena.clubList.push(this.clubInput.allList[i]);
      } else {
          alert('not a FSF club');
      }
      this.clubInput.filter = '';
      return false;
  }

  /**
   * If there are no form errors, send the data to the server.
   */
  saveArena() {
    if (this.form.valid) {
      this.formState = FormState.Saving;
      this.arenaSaveState = FormState.Saving;
      this.filesSaveState = FormState.Saving;
      if (!this.arena.id) {
        // create new arena
        this.fsfService.createArena(this.arena).subscribe(
                data  => {
                          this.resetArenaForm(data[0]);
                          // we have the id of the created arena, save the files
                          this.fileUploadComponent.uploadAllFiles({arena: this.arena.id});
                          this.regulationsFileUploadComponent.uploadAllFiles({arena: this.arena.id, attribute: 1});
                          this.arenaSaveState = FormState.Saved;
                          this.formSaveStateChanged();
                         },
                error => {
                          this.arenaSaveState = FormState.FailedSubmission;
                          this.formState = FormState.FailedSubmission;
                         });
      } else {
          // update existing
          this.fsfService.updateArena(this.arena).subscribe(
                  data  => {
                            this.resetArenaForm(data[0]);
                            this.arenaSaveState = FormState.Saved;
                            this.formSaveStateChanged();
                           },
                           error => {
                               this.arenaSaveState = FormState.FailedSubmission;
                               this.formState = FormState.FailedSubmission;
                              });
          // upload files
          this.fileUploadComponent.uploadAllFiles({arena: this.arena.id});
          this.regulationsFileUploadComponent.uploadAllFiles({arena: this.arena.id, attribute: 1});
      }
    } else {
        this.formState = FormState.FailedSubmission;
    }
  }

  /**
   * Called when the value of any input field, including file upload, is changed.
   * Updates formState to Unsaved
   * FailedSubmission is a sticky state
   */
  formInputChange() {
    if (this.formState !== FormState.FailedSubmission) {
      this.formState = FormState.Unsaved;
    }
  }

  /**
   * During form submission, updates this.formState based on this.arenaSaveState and this.filesSaveState
   */
  private formSaveStateChanged() {
      if (this.formState === FormState.Saving) {
          if (this.arenaSaveState === FormState.Saved && this.filesSaveState === FormState.Saved ) {
              this.formState = FormState.Saved;
          } else {
              if (this.arenaSaveState === FormState.FailedSubmission || this.filesSaveState === FormState.FailedSubmission) {
                  this.formState = FormState.FailedSubmission;
              }
          }
      }
  }

  /**
   * Called after all file uploads have finished.
   */
  onFilesSaved() {
      this.filesSaveState = FormState.Saved;
      this.formSaveStateChanged();
      this.loadAttatchments();
  }

  private loadAttatchments() {
    this.fsfService.getAttachmentList(this.arena.id).subscribe((list) => {
      this.imageList = [];
      list.forEach( e => {
        if (e.attribute_id === null) {
          this.imageList.push(e);
        } else {
          if (!this.files[e.attribute_id]) {
            this.files[e.attribute_id] = [];
          }
          this.files[e.attribute_id].push(e);
        }
      } );
    });
  }

  /**
   * Update the form with the values returned by the server.
   * Sets form state to Pristine
   * Called from ngOnInit() and and saveArena((), i.e. after the server returns the value for an arena (get, create, update).
   * @param arena
   */
  private resetArenaForm(arena: Arena) {
    this.form.reset(arena);
    this.arena.id = arena.id; // id is not part of the form, set it manually
    this.arena.clubList = arena.clubList; // clubList is not part of the form, set it manually

    this.formState = FormState.Pristine;
    this.clubInput = {
            filter: '',
            filteredList: [],  // all clubs matching the text written in the club input field
            allList: this.clubInput.allList        // the list of clubs fetched from the server
       };
    // TODO race condition, files might not be uploaded during save
    // fetch list of attachments
    if (arena.id) {
      this.loadAttatchments();
    }
  }

  /**
   * Open the dialog for dd mm ss inout of GPS coordinates
   */
  public openMapDialog(event) {
   event.stopPropagation();
   const dialogRef = this.dialog.open(DialogMapDialog, {
     data: this.arena,
     height: (document.documentElement.clientHeight - 30) + 'px',
     width: (document.documentElement.clientWidth - 30) + 'px'
   });
  }

  public getDisplayClass(value) {
    return value || this.currentUser ? '' : 'fsf-hidden';
  }
}
