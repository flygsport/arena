import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { MatAutocompleteModule,
         MatButtonModule,
         MatCardModule,
         MatCheckboxModule,
         MatDialogModule,
         MatDividerModule,
         MatExpansionModule,
         MatFormFieldModule,
         MatIconModule,
         MatInputModule,
         MatListModule,
         MatPaginatorModule,
         MatSelectModule,
         MatSortModule,
         MatSidenavModule,
         MatTableModule,
         MatTabsModule,
         MatToolbarModule,
         MatTooltipModule
         } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { StoreModule } from '@ngrx/store';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';

import { SidenavModule } from './sidenav/sidenav.module';

import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ArenaFormComponent } from './arena-form/arena-form.component';
import { FileUploadComponent } from './file-upload/';

import { AuthModule, AuthConfig } from '@holmby/auth';

import { environment } from '../environments/environment';

import { reducers, metaReducers } from './reducers';

import { FSFService } from './services/fsf.service';
import { ServerInfoService } from './services/server.info.service';
import { ClubStore } from './services/club.service';

import { AboutComponent } from './about/about.component';
import { GPSDecimalDegreeDirective } from './validators/gps-decimal-degree.directive';
import { IsNumericValidatorDirective } from './validators/is-numeric-validator.directive';
import { IntegerValidatorDirective } from './validators/integer-validator.directive';
import { ListArenasComponent, DialogAlternativeNameDialogComponent } from './list-arenas/list-arenas.component';
import { CarouselComponent } from './carousel/carousel.component';
import { SlideComponent } from './carousel/slide.component';
import { MapComponent } from './map/map.component';
import { DialogMapDialog } from './map/map.dialog.component';
import { ArenaFilterComponent } from './arena-filter/arena-filter.component';
import { ArenaFilterStore } from './services/arena-filter.service';
import { ExportExelDialogComponent } from './export-exel-dialog/export-exel-dialog.component';
import { AppEffects } from './app.effects';
import { ClubModule } from './club/club.module';
import { ListClubsComponent } from './club/list-clubs/list-clubs.component';
import { ViewClubComponent } from './club/view-club/view-club.component';
import { ClubFormComponent } from './club/club-form/club-form.component';

const authConfig: AuthConfig = {
  persistent: true,               // store login info in localstore (restore login state when reloading page)

  inactivWarningTime: 25 * 60 * 1000,    // after inactivWarningTime miliseconds of inactivity a warning is shown
  inactivLogoutTime: 30 * 60 * 1000,     // after inactivLogoutTime miliseconds of inactivity the user is logged out

  text: {
    loginUser: 'användare',
    loginPassword: 'lösenord'
  },

  url: {
    login: '/rest/holmby/v1/login.holmby.php',
    forgotPassword: '/rest/holmby/v1/forgotPassword.php',
  }
};

  const appRoutes: Routes = [
    { path: 'about', component: AboutComponent },
    { path: 'new', component: ArenaFormComponent },
    { path: 'show/:id', component: ArenaFormComponent },
    { path: 'list', component: ListArenasComponent },
    { path: 'map', component: MapComponent },
    { path: 'viewOnMap/:id', component: MapComponent },

    { path: 'new-club', component: ClubFormComponent },
    { path: 'list-clubs', component: ListClubsComponent },
    { path: 'club/:id', component: ClubFormComponent },

    { path: '',
        redirectTo: '/list',
        pathMatch: 'full'
      },
    { path: '**', component: PageNotFoundComponent }
  ];

@NgModule({
  entryComponents: [
    DialogAlternativeNameDialogComponent,
    DialogMapDialog,
    ExportExelDialogComponent
  ],
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    ArenaFormComponent,
    AboutComponent,
    GPSDecimalDegreeDirective,
    IsNumericValidatorDirective,
    IntegerValidatorDirective,
    ListArenasComponent,
    DialogAlternativeNameDialogComponent,
    FileUploadComponent,
    CarouselComponent, SlideComponent,
    MapComponent,
    DialogMapDialog,
    ArenaFilterComponent,
    ExportExelDialogComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,

    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatPaginatorModule,
    MatSelectModule,
    MatSidenavModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,

    SidenavModule,

    AuthModule.forRoot(authConfig),

    RouterModule.forRoot(appRoutes, { useHash: true }),
    BrowserAnimationsModule,

    StoreModule.forRoot(reducers, { metaReducers }),
    StoreRouterConnectingModule.forRoot( { stateKey: 'router' } ),
    environment.production ? [] : StoreDevtoolsModule.instrument(),
    EffectsModule.forRoot([AppEffects]),

    ClubModule,
  ],
  providers: [
    FSFService,
    ServerInfoService,
    ClubStore,
    ArenaFilterStore
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
