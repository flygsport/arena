import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { ArenaTableDataSource } from '../list-arenas/list-arenas.component';

@Component({
  selector: 'app-export-exel-dialog',
  templateUrl: './export-exel-dialog.component.html',
  styleUrls: ['./export-exel-dialog.component.scss']
})
export class ExportExelDialogComponent implements OnInit {

  public panelOpenState = false;
  public export = [];
  public fileName = 'arenor.xlsx';
  rowCount = 0;     // number of arenas to export

  constructor(@Inject(MAT_DIALOG_DATA) public data: { dataSource: ArenaTableDataSource, nameMap: [{src: string, dst: string}]}) { }

  ngOnInit() {
    // default, export all attributes
    this.setBoxValue(true);
    // cout the number of exproted arenas
    this.data.dataSource.data.forEach(arena => {
      if (this.data.dataSource.filterMethod(arena, null)) {
        this.rowCount++;
      }
    });

  }

  onSave() {
    const exportMap = [];
    this.data.nameMap.forEach( map => {
      if (this.export[map.src]) {
        exportMap.push(map);
      }
    });
    this.data.dataSource.saveAsExcel(this.fileName, exportMap);
  }

  setBoxValue(value) {
    this.data.nameMap.forEach( map => {
      this.export[map.src] = value;
    });
  }
}
