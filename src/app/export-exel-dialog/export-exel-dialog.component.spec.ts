import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExportExelDialogComponent } from './export-exel-dialog.component';

describe('ExportExelDialogComponent', () => {
  let component: ExportExelDialogComponent;
  let fixture: ComponentFixture<ExportExelDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExportExelDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExportExelDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
