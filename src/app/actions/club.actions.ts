import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Club } from '../services/fsf.service.types';

export enum ClubActionTypes {
  LOAD_CLUBS_SUCCESS    = 'Load Clubs Success',
  LOAD_CLUBS_FAIL       = 'Load Clubs Fail',

  CREATE_CLUB_REQUEST = 'Create Club Request',
  CREATE_CLUB_SUCCESS = 'Create Club Success',
  CREATE_CLUB_FAIL    = 'Create Club Fail',

  UPDATE_CLUB_REQUEST = 'Update Club Request',
  UPDATE_CLUB_SUCCESS = 'Update Club Success',
  UPDATE_CLUB_FAIL    = 'Update Club Fail',
}

// --- Load Club ---
export class LoadClubsSuccess implements Action {
  readonly type = ClubActionTypes.LOAD_CLUBS_SUCCESS;
  constructor(public payload: Club[]) { }
}
export class LoadClubsFail implements Action {
  readonly type = ClubActionTypes.LOAD_CLUBS_FAIL;
  // payload is an error message intended to be displayed in the GUI
  constructor(public payload: string) { }
}

// --- Crate Club ---
export class CreateClubRequest implements Action {
  readonly type = ClubActionTypes.CREATE_CLUB_REQUEST;
  constructor(public payload: Club) { }
}
export class CreateClubFail implements Action {
  readonly type = ClubActionTypes.CREATE_CLUB_FAIL;
  // payload is an error message intended to be displayed in the GUI
  constructor(public payload: string) { }
}
export class CreateClubSuccess implements Action {
  readonly type = ClubActionTypes.CREATE_CLUB_SUCCESS;
  constructor(public payload: Club) { }
}

// --- Update Club ---
export class UpdateClubRequest implements Action {
  readonly type = ClubActionTypes.UPDATE_CLUB_REQUEST;
  constructor(public payload: Club) { }
}
export class UpdateClubFail implements Action {
  readonly type = ClubActionTypes.UPDATE_CLUB_FAIL;
  // payload is an error message intended to be displayed in the GUI
  constructor(public payload: string) { }
}
export class UpdateClubSuccess implements Action {
  readonly type = ClubActionTypes.UPDATE_CLUB_SUCCESS;
  constructor(public payload: Club) { }
}

export type ClubActions = LoadClubsSuccess | LoadClubsFail |
                          CreateClubRequest | CreateClubFail | CreateClubSuccess |
                          UpdateClubRequest | UpdateClubFail | UpdateClubSuccess;
